//
//  AppDelegate.swift
//  KhanjiMark
//
//  Created by Apple on 31/05/21.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import AppInvokeSDK
import Firebase
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let sharedAppDelegate = AppDelegate()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        if UserDefaults.standard.value(forKey: userdefault().userProfile) != nil {
            userProfile = getCachedUserJSONData()!
            print("name \(userProfile[userProfileKeys().first_name])")
        }
        goToRootView()
        return true
    }

    
    func goToRootView() {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            var homecontroller = UIViewController()
        if UserDefaultsManager().isLogIn ?? false {
            print("user type \(UserType().Customer)")
            print("userprofile  type \(userProfile[userProfileKeys().userType].stringValue)")
            if userProfile[userProfileKeys().userType].stringValue == UserType().Customer {
                homecontroller = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as!  SWRevealViewController
            } else {
                homecontroller = storyboard.instantiateViewController(withIdentifier: "SuppilerTabBarVC") as!  SuppilerTabBarVC
             
            }
        } else {
            homecontroller = storyboard.instantiateViewController(withIdentifier: "IntialVC") as! IntialVC
        }
//        homecontroller = storyboard.instantiateViewController(withIdentifier: "PaymentHistoryVC") as! PaymentHistoryVC
            
            let homeNavi = UINavigationController(rootViewController: homecontroller)
            homeNavi.navigationBar.isHidden = true
            homeNavi.interactivePopGestureRecognizer?.isEnabled = false
            self.window?.rootViewController = homeNavi
            self.window?.makeKeyAndVisible()
            
        }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let dict = url.absoluteString
        print("dict \(dict)")
        //get paytm params from dictionary
        return true
    }
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "KhanjiMark")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

