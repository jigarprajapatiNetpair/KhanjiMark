//
//  CheckOutStepFourPromocodeVC.swift
//  KhanjiMark
//
//  Created by RMV on 8/29/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import AppInvokeSDK

class CheckOutStepFourPromocodeVC: UIViewController,AIDelegate {
    func openPaymentWebVC(_ controller: UIViewController?) {
            if let vc = controller {
                DispatchQueue.main.async {[weak self] in
                    self?.present(vc, animated: true, completion: nil)
                }
            }
        }
    
    @IBOutlet weak var topView: UIView!
    var selectedParamter:Parameters = [:]
    var selectedProduct = JSON()
    var cartDataList = [JSON]() 
    @IBOutlet weak var promoTxt: UITextField!
    
    @IBOutlet weak var subTotalPrice: UILabel!
    
    
    @IBOutlet weak var discountLbl: UILabel!
    
    
    @IBOutlet weak var totalPriceLbl: UILabel!
    
    var counpeID = ""
    var coupenDiscount = ""
    var TotalPrice = ""
    var TotalFinaltotalPrice = ""
    private let appInvoke = AIHandler()

    override func viewDidLoad() {
        super.viewDidLoad()
        topView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = 40
        topView.clipsToBounds = true
        
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        TotalFinaltotalPrice = TotalPrice
        if let formattedTipAmount = formatter.string(from: Double("\(TotalPrice)") as! NSNumber) {
//            cell.priceLbl.text = "\(formattedTipAmount)"
            print("formattedTipAmount \(formattedTipAmount)")
            
            subTotalPrice.text = formattedTipAmount
            totalPriceLbl.text = formattedTipAmount
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    func didFinish(with status: AIPaymentStatus, response: [String : Any]) {
        print("🔶 Paytm Callback Response: ", response)
    }

    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func placeOrderBtnTap(_ sender: UIButton) {
//
//        let callback = callBackURL + "123456"
//        let url = URLS.paytm().intialTokenGet
//
//        let parameter:Parameters = [
//            "code":marchantcode,
//            "MID":merchantId,
//            "ORDER_ID":"123456",
//            "AMOUNT":TotalFinaltotalPrice
//        ]
//        print("parameter \(parameter)")
//        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
//
//            if json["body"]["resultInfo"]["resultMsg"].stringValue != "" {
//
//                let body = json["body"].self
//                print("json \(body["txnToken"].stringValue)")
//
//                self.appInvoke.openPaytm(merchantId: merchantId, orderId: "12345", txnToken: body["txnToken"].stringValue,
//                                                 amount: "200", callbackUrl: callback, delegate: self, environment: .staging)
//            }
//        }
//
        
        
        
        
        var orderid = ""

        for item in cartDataList {
            orderid.append("\(item["cart_item_id"].stringValue),")
        }
        orderid.removeLast()
        let parameter:Parameters = [
            "cart_id":orderid,
            "coupon_code_apply_id":self.counpeID,
            "coupon_code_apply_discount":self.coupenDiscount
        ]
        let url = URLS.Consumer().placeOrder
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
            if json["status"].boolValue == true {
                AppDelegate.sharedAppDelegate.goToRootView()
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in

                }
            }
        }
        
    }
    
    
    
    @IBAction func applyPromoCodeBtn(_ sender: UIButton) {
        let url = URLS.paytm().couponCodeValidation
        
        let parameter:Parameters = [
//            "ORDER_ID":"123456",
            "coupon_code":promoTxt.text ?? ""
        ]
        
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
           
            if json["status"].boolValue == true {
                let couponData = json["data"].self
                print("couponData \(couponData)")
                self.counpeID = couponData["id"].stringValue
                
                
                let dateFormate = DateFormatter()
                dateFormate.dateFormat = "yyyy-MM-dd"
                let expireData = dateFormate.date(from: "\(couponData["expire_date"].stringValue)")
                
                if expireData! < Date() {
                    
                        Toast.show(message: "your coupen has been experi", controller: self) { status in
                        //
                                        }
                } else {
                    if couponData["type"].stringValue == "fix" {
                        let price = Double("\(self.TotalPrice)")
                        print("price \(price)")
                        let discount = Double("\(couponData["value"].stringValue)")
                        self.coupenDiscount = "\(discount ?? 0.0)"
                        let formatter = NumberFormatter()
                        formatter.locale = Locale.current
                        formatter.numberStyle = .currency
                      
                        
                        
                        if let formattedTipAmount = formatter.string(from: discount as! NSNumber) {
                            self.discountLbl.text = formattedTipAmount
                        }
                        
                        
                        
                        let finalPrice = CGFloat(price!) - CGFloat(discount!)
                        self.TotalFinaltotalPrice = "\(finalPrice)"
                        if let formattedTipAmount = formatter.string(from: Double("\(finalPrice)") as! NSNumber) {
                            print("formattedTipAmount \(formattedTipAmount)")
                            self.totalPriceLbl.text = formattedTipAmount
                        }
                    } else {
                        
                        
                        let price = Double("\(self.TotalPrice)")
                        print("price \(price)")
                        let discount = calculatePercentage(value: price ?? 0.0, percentageVal: 0.7)
                        self.coupenDiscount = "\(discount ?? 0.0)"
                        let formatter = NumberFormatter()
                        formatter.locale = Locale.current
                        formatter.numberStyle = .currency
                      
                        
                        
                        if let formattedTipAmount = formatter.string(from: discount as! NSNumber) {
                            self.discountLbl.text = formattedTipAmount
                        }
                        
                        
                        
                        let finalPrice = CGFloat(price!) - CGFloat(discount)
                        self.TotalFinaltotalPrice = "\(finalPrice)"
                        if let formattedTipAmount = formatter.string(from: Double("\(finalPrice)") as! NSNumber) {
                            print("formattedTipAmount \(formattedTipAmount)")
                            self.totalPriceLbl.text = formattedTipAmount
                        }                    }
                }
                
                
                
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                //
                                }
            }
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
