//
//  CheckOutStepFirstVC.swift
//  KhanjiMark
//
//  Created by RMV on 7/25/21.
//

import UIKit
import DropDown
import SwiftyJSON
import Alamofire
import WWCalendarTimeSelector

class CheckOutStepFirstVC: UIViewController, WWCalendarTimeSelectorProtocol, UITextViewDelegate, UITextFieldDelegate {

    
    @IBOutlet weak var topView: UIView!

    
    @IBOutlet weak var qtyDropDown: UIButton!
    @IBOutlet weak var qtyTxt: DesignableUITextField!
    
    
    
    
    @IBOutlet weak var districtLbl: UILabel!
    @IBOutlet weak var talukoLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var deliveryAddress: UITextView!
    @IBOutlet weak var contactNumber: DesignableUITextField!
    @IBOutlet weak var deliverDateLbl: UILabel!
    
    @IBOutlet weak var districtBtn: UIButton!
    @IBOutlet weak var talukoBtn: UIButton!
    @IBOutlet weak var cityBtn: UIButton!
    
    var selectQtyString = ["1","2","3","4","5","6","7","8","9","10","Add Custom"]
    
    var selectQtydrop = DropDown()
    
    var SiteDistrictJSon = [JSON]()
    var SitetakulaJSon = [JSON]()
    var SitePinCodeJSon = [JSON]()
    
    
    var SiteDistrictString:[String] = []
    let siteDistrictDrop = DropDown()
    var selectedSiteDistricut = ""
    
    var SiteTakulaString:[String] = []
    let siteTakuloDrop = DropDown()
    var selectedSiteTaluka = ""
    
    var SitePinCodeString:[String] = []
    let sitePinCodeDrop = DropDown()
    var selectedSitePin = ""
    
    var selectedProduct = JSON()
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Delivery Address"
            textView.textColor = UIColor.lightGray
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        qtyTxt.delegate = self
        deliveryAddress.delegate = self
        deliveryAddress.text = "Delivery Address"
        deliveryAddress.textColor = .lightGray
        getDataFromAPI()
        setupQtyData()
        topView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = CGFloat(Double(topView.frame.height / 2))
        topView.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    
    func setupQtyData() {
        qtyDropDown.setTitle( "", for: .normal)
        selectQtydrop.anchorView = qtyDropDown  // UIView or UIBarButtonItem
        selectQtydrop.dataSource = selectQtyString
        selectQtydrop.selectionAction = { [unowned self] (index: Int, item: String) in
            
            if item == "Add Custom" {
                qtyDropDown.isHidden = true
                qtyTxt.becomeFirstResponder()
            } else  {
                qtyTxt.text = item
            }
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == qtyTxt {
            qtyDropDown.isHidden = false
        }
    }
    
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func districtBtnTap(_ sender: UIButton) {
        siteDistrictDrop.show()
    }
    
    
    
    @IBAction func qtyDropBtnTap(_ sender: UIButton) {
        selectQtydrop.show()
    }
    
    
    func showCalendar() {
            let selector = WWCalendarTimeSelector.instantiate()
            selector.delegate = self
        
            /*
              Any other options are to be set before presenting selector!
            */
        present(selector, animated: true, completion: nil)
        }
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print(date)
        let dateFor = DateFormatter()
        dateFor.dateFormat = "yyyy-MM-dd hh:mm:ss"
        deliverDateLbl.text = dateFor.string(from: date)
        deliverDateLbl.textColor = .black
    }

    @IBAction func talukoBtnTap(_ sender: UIButton) {
        siteTakuloDrop.show()
    }
    
    
    @IBAction func cityBtnTap(_ sender: UIButton) {
        sitePinCodeDrop.show()
    }
    
    
    @IBAction func deliberyDateBtnTap(_ sender: UIButton) {
        showCalendar()
    }
    
    
    @IBAction func nextBtnTap(_ sender: UIButton) {
       print("selectedProduct \(selectedProduct)")
        if validation() {
        let parameter:Parameters = [
            "state_id":5,
            "district_id":selectedSiteDistricut,
            "taluka_id":selectedSiteTaluka,
            "pincode_id":selectedSitePin,
            "product_id":selectedProduct["id"].stringValue,
            "qty":qtyTxt.text ?? "",
            "delivery_address":deliveryAddress.text ?? "",
            "contact_number":contactNumber.text ?? "",
            "expected_delivery_date":deliverDateLbl.text ?? "",
        ]
            print("parameter \(parameter)")
            let stepTwo = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutStepSecondVC") as! CheckOutStepSecondVC
            stepTwo.selectedProduct = selectedProduct
            stepTwo.selectedParamter = parameter
            self.navigationController?.pushViewController(stepTwo, animated: true)
            
            
        }
        
    }
    
    func validation() -> Bool {
        if qtyTxt.text == "" {
            Toast.show(message: "Please enter quantity", controller: self) { status in
                
            }
            return false
        } else if districtLbl.text == "" {
            Toast.show(message: "Please select district name", controller: self) { status in
                
            }
            return false
        } else if talukoLbl.text == "" {
            Toast.show(message: "Please select taluka name", controller: self) { status in
                
            }
            return false
        } else if cityLbl.text == "" {
            Toast.show(message: "Please select pincode", controller: self) { status in
                
            }
            return false
        } else if deliveryAddress.text == "" {
            Toast.show(message: "Please enter Delivery Address", controller: self) { status in
                
            }
            return false
        } else if contactNumber.text == "" {
            Toast.show(message: "Please enter contact number", controller: self) { status in
                
            }
            return false
        } else if deliverDateLbl.text == "" {
            Toast.show(message: "Please select Delivery Date", controller: self) { status in
                
            }
            return false
        } else {
            return true
        }
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CheckOutStepFirstVC {
    
    func setupUpDropDown() {
    SetSiteDistrictDropDown()
    SetSiteTalukaDropDown()
    SetSitePinDropDown()
        
    }
    
    func SetSiteDistrictDropDown() {
        
        siteDistrictDrop.anchorView = districtBtn  // UIView or UIBarButtonItem
        siteDistrictDrop.dataSource = SiteDistrictString
        siteDistrictDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            districtLbl.text = item
            districtLbl.textColor = .black

            
            talukoLbl.text = "select taluko"
            cityLbl.text = "select pin code"
            
            talukoLbl.textColor = .lightGray
            cityLbl.textColor = .lightGray
            
            selectedSiteDistricut = SiteDistrictJSon[index]["id"].stringValue
            getSiteTalukaData()
            siteDistrictDrop.hide()
        }
        
    }
    
    func SetSiteTalukaDropDown() {
        siteTakuloDrop.anchorView = talukoBtn  // UIView or UIBarButtonItem
        siteTakuloDrop.dataSource = SiteTakulaString
        siteTakuloDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            talukoLbl.text = item
            talukoLbl.textColor = .black
            cityLbl.text = "select pin code"
            cityLbl.textColor = .lightGray
            selectedSiteTaluka = SitetakulaJSon[index]["id"].stringValue
            getSitePinCodeData()
            siteTakuloDrop.hide()
        }
        
    }
    
    func SetSitePinDropDown() {
        sitePinCodeDrop.anchorView =  cityBtn// UIView or UIBarButtonItem
        sitePinCodeDrop.dataSource = SitePinCodeString
        sitePinCodeDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            cityLbl.text = item
            cityLbl.textColor = .black

            selectedSitePin = SitePinCodeJSon[index]["id"].stringValue
            sitePinCodeDrop.hide()
        }
        
    }
    
    
    
    func getDataFromAPI() {
        let siteDistrictParameter = [
            "state_id":"5"
        ]
        
        NetworkManager().getDataFromServer(url: URLS.Comman().GetDistrict, method: .post, parameter: siteDistrictParameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.SiteDistrictJSon = json["data"].arrayValue
                self.SiteDistrictString.removeAll()
                for item in self.SiteDistrictJSon {
                    
                    self.SiteDistrictString.append(item["district"].stringValue)
                }
                self.SetSiteDistrictDropDown()
//                self.getSiteTalukaData()
                
                
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
            
        }
        
        
    }
    
    func getSiteTalukaData() {
        
            let siteTalukaParameter = [
                "district_id":self.selectedSiteDistricut
            ]
            
            NetworkManager().getDataFromServer(url: URLS.Comman().GetTaluka, method: .post, parameter: siteTalukaParameter, view: self.view) { json in
                
                if json["status"].boolValue == true {
                    self.SitetakulaJSon = json["data"].arrayValue
                    self.SiteTakulaString.removeAll()
                    for item in self.SitetakulaJSon {
                        self.SiteTakulaString.append(item["taluka"].stringValue)
                    }
                    self.SetSiteTalukaDropDown()
//                    self.getSitePinCodeData()
                    
                } else {
                    Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                        
                    }
                }
                
            }
        
    }
    
    func getSitePinCodeData() {
        let sitePinCodeParameter = [
            "district_id":self.selectedSiteDistricut,
            "taluka_id":self.selectedSiteTaluka
        ]
        
        NetworkManager().getDataFromServer(url: URLS.Comman().GetPincode, method: .post, parameter: sitePinCodeParameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.SitePinCodeJSon = json["data"].arrayValue
                self.SitePinCodeString.removeAll()
               
                for item in self.SitePinCodeJSon {
                    self.SitePinCodeString.append("\(item["name"].stringValue)(\(item["pincode"].stringValue))")
                }
                self.SetSitePinDropDown()
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
            
        }
        
    }
    
    
}
