//
//  CheckOutStepSecondVC.swift
//  KhanjiMark
//
//  Created by RMV on 7/25/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import ImageSlideshow

class CheckOutStepSecondVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, ImageSlideshowDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        supplierListData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let TopSupplier = collectionView.dequeueReusableCell(withReuseIdentifier: "TopSupplierCollectionCell", for: indexPath) as! TopSupplierCollectionCell
        TopSupplier.supplierCode.text = supplierListData[indexPath.row]["supplier_code"].stringValue
        TopSupplier.supplierImg.imageFromUrl(message: "", urlString: supplierListData[indexPath.row]["profile_photo"].stringValue) { staus in
            
        }
        TopSupplier.supplierProductBtn.tag = indexPath.row
        TopSupplier.ViewImgBtn.tag = indexPath.row
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: Double("\(supplierListData[indexPath.row]["special_price"].stringValue)") as! NSNumber) {
//            cell.priceLbl.text = "\(formattedTipAmount)"
            print("formattedTipAmount \(formattedTipAmount)")
            TopSupplier.supplierCount.text = formattedTipAmount
        }
        
        return TopSupplier
    }
    
    var selectedProduct = JSON()

    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
            return .portrait
        }
    
    let slideshow =  ImageSlideshow()
    
    @IBOutlet weak var supplierList: UICollectionView!
    var selectedParamter:Parameters = [:]
    var supplierListData = [JSON]() {
        didSet {
            if supplierListData.count > 0 {
                supplierList.isHidden = false
                supplierList.delegate = self
                supplierList.dataSource = self
            } else {
                supplierList.isHidden = true
            }
        }
    }
    @IBOutlet weak var topView: UIView!
    var alamofireSourceList:[AlamofireSource] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getSupplierList()
        
        topView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = CGFloat(Double(topView.frame.height / 2))
        topView.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    func getSupplierList() {
        let url = URLS.Consumer().getSupplierList
        let parameter:Parameters = [
        
            "district_id":selectedParamter["district_id"] ?? "",
            "taluka_id":selectedParamter["taluka_id"] ?? "",
            "pincode_id":selectedParamter["pincode_id"] ?? "",
            "product_id":selectedParamter["product_id"] ?? "",
            "qty":selectedParamter["qty"] ?? "",
        ]
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.supplierListData = json["data"].arrayValue

            } else {
//                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
//
//                }
            }
            
        }
    }
    
    @IBAction func viewImgbtn(_ sender: UIButton) {
        
        
        
        let url = URLS.Consumer().getSupplierListImage
        let parameter:Parameters = [
        
            "product_id":selectedProduct["id"] ?? "",
            "user_id":self.supplierListData[sender.tag]["supplier_id"].stringValue
        ]
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                let imageArrya = json["data"].arrayValue
                
                self.slideshow.slideshowInterval = 5.0
                self.slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
                self.slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
                self.slideshow.pageIndicator = UIPageControl()
                
                

                self.slideshow.activityIndicator = DefaultActivityIndicator()
                self.slideshow.delegate = self
                print("selectedProduct \(self.selectedProduct)")
                
                
//                let images = (self.imageArrya["product_images"].object as? [String])!
                self.alamofireSourceList.removeAll()
                for item in imageArrya {
                    self.alamofireSourceList.append(AlamofireSource(urlString: item["image"].stringValue)!)
                }
                self.slideshow.setImageInputs(self.alamofireSourceList)
                let fullScreenController = self.slideshow.presentFullScreenController(from: self)
                       // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
                    fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
                
            } else {
//                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
//
//                }
            }
            
        }
        
        
        
        
        
        
        
    }
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectSuppliers(_ sender: UIButton) {
        let selectedId = supplierListData[sender.tag]["supplier_id"].stringValue
        selectedParamter.updateValue(selectedId, forKey: "suppliers_id")
        selectedParamter.updateValue(userProfile[userProfileKeys().id].stringValue, forKey: "user_id")
        
        let totalAmount = (Int("\(selectedParamter["qty"] ?? "")") ?? 0) * (Int("\(supplierListData[sender.tag]["special_price"].stringValue)") ?? 0)
        selectedParamter.updateValue(totalAmount, forKey: "amount")
        selectedParamter.updateValue("0", forKey: "tax")
        selectedParamter.updateValue("\(selectedParamter["pincode_id"] ?? "")", forKey: "pincode")
        selectedParamter.removeValue(forKey: "pincode_id")
       
        selectedParamter.updateValue("\(selectedParamter["qty"] ?? "")", forKey: "quantity")
        selectedParamter.removeValue(forKey: "qty")
        
//        print("selectedParamter \(selectedParamter)")
//
//                        let promoCodeVC = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutStepFourPromocodeVC") as! CheckOutStepFourPromocodeVC
//                        promoCodeVC.selectedProduct = selectedProduct
//                        promoCodeVC.selectedParamter = selectedParamter
//                        self.navigationController?.pushViewController(promoCodeVC, animated: true)
//
        
        addToCard()
    }
    func addToCard() {


        let url =  URLS.Consumer().addCart
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: selectedParamter, view: self.view) { json in
            print("json \(json)")
            if json["status"].boolValue == true {

                let cardList = self.storyboard?.instantiateViewController(withIdentifier: "ConsumerCardListVC") as! ConsumerCardListVC
                self.navigationController?.pushViewController(cardList, animated: true)
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in

                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
