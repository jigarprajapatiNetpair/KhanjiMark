//
//  ConsumerSideMenuVC.swift
//  KhanjiMark
//
//  Created by Apple on 10/06/21.
//

import UIKit
import PMAlertController

var isShopeEnable = false
var isSuppliersEnable = false
var isOrderListEnable = false

class ConsumerSideMenuVC: UIViewController {

    @IBOutlet weak var userBannerImg: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.text = "\(userProfile[userProfileKeys().first_name].stringValue) \(userProfile[userProfileKeys().last_name].stringValue)"
        
        userName.textColor = .black
        print("userProfile \(userProfile[userProfileKeys().profilePhoto].stringValue)")
        userImg.imageFromUrl(message: "", urlString: userProfile[userProfileKeys().profilePhoto].stringValue) { (status) in
            
        }
        userBannerImg.imageFromUrl(message: "", urlString: userProfile[userProfileKeys().profilePhoto].stringValue) { (status) in
            
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeBtnTap(_ sender: UIButton) {
        self.revealViewController()?.revealToggle(animated: true)
    }
    @IBAction func logoutBtnTap(_ sender: UIButton) {
        let alertVC = PMAlertController(title: "Logout", description: "Are you sure!!", image: nil, style: .alert)

        alertVC.addAction(PMAlertAction(title: "Cancel", style: .cancel, action: { () -> Void in
                    print("Capture action Cancel")
                }))

        alertVC.addAction(PMAlertAction(title: "YES", style: .default, action: { () in
            print("Capture action ok")
                    UserDefaultsManager().isLogIn = false
            AppDelegate.sharedAppDelegate.goToRootView()
                }))

        self.present(alertVC, animated: true, completion: nil)
    }
    @IBAction func shopeBtnTap(_ sender: UIButton) {
        
        isShopeEnable = true
        self.revealViewController()?.revealToggle(animated: true)
    }
    
    @IBAction func supplierBtnTap(_ sender: UIButton) {
        isSuppliersEnable = true
        self.revealViewController()?.revealToggle(animated: true)
    }
    
    @IBAction func orderbtnTap(_ sender: UIButton) {
        isOrderListEnable = true
        self.revealViewController()?.revealToggle(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
