//
//  ConsumerTopSupplierVC.swift
//  KhanjiMark
//
//  Created by Apple on 11/06/21.
//

import UIKit
import SwiftyJSON
class ConsumerTopSupplierVC: UIViewController {
    @IBOutlet weak var supplierCollection: UICollectionView!
    @IBOutlet weak var topView: UIView!
    var supplierList = [JSON]()
    override func viewDidLoad() {
        super.viewDidLoad()
        topView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = 40
        topView.clipsToBounds = true
        setCollectionView()
        // Do any additional setup after loading the view.
    }
    func setCollectionView() {
        supplierCollection.delegate = self
        supplierCollection.dataSource = self
        supplierCollection.reloadData()
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func viewProduct(_ sender: UIButton) {
        let productListVC = self.storyboard?.instantiateViewController(withIdentifier: "ConsumerProductListVC") as! ConsumerProductListVC
        productListVC.whereToCome = "TopSupplier"
        productListVC.selectedCategoryID = supplierList[sender.tag]["id"].stringValue
        self.navigationController?.pushViewController(productListVC, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ConsumerTopSupplierVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return supplierList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let TopSupplier = collectionView.dequeueReusableCell(withReuseIdentifier: "TopSupplierCollectionCell", for: indexPath) as! TopSupplierCollectionCell
        TopSupplier.supplierCode.text = supplierList[indexPath.row]["supplier_code"].stringValue
        TopSupplier.supplierImg.imageFromUrl(message: "", urlString: supplierList[indexPath.row]["profile_photo"].stringValue) { staus in
            
        }
        TopSupplier.supplierProductBtn.tag = indexPath.row
        TopSupplier.supplierCount.text = supplierList[indexPath.row]["count"].stringValue
        return TopSupplier
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/3.0
        
        return CGSize(width: width - 10, height: 190)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 0, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    
}

