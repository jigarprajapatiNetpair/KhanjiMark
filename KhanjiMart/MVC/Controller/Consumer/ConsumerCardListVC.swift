//
//  ConsumerCardListVC.swift
//  KhanjiMark
//
//  Created by RMV on 7/11/21.
//

import UIKit
import SwiftyJSON
import Alamofire
import AppInvokeSDK
class ConsumerCardListVC: UIViewController,AIDelegate {
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var cartTbl: UITableView!
    @IBOutlet weak var placeOrderHeight: NSLayoutConstraint!//48
    @IBOutlet weak var placeOrderTop: NSLayoutConstraint!//16
    @IBOutlet weak var placeOrderBottom: NSLayoutConstraint!
    
    @IBOutlet weak var placeOrderBtn: UIButton!
    var cartDataList = [JSON]() {
        didSet {
            if cartDataList.count > 0 {
                placeOrderHeight.constant = 48
                placeOrderTop.constant = 16
                placeOrderBottom.constant = 16
                placeOrderBtn.isHidden = false
                
                noDataView.isHidden = true
                cartTbl.isHidden = false
                cartTbl.delegate = self
                cartTbl.dataSource = self
                cartTbl.reloadData()
            } else {
                
                placeOrderHeight.constant = 0
                placeOrderTop.constant = 0
                placeOrderBottom.constant = 0
                placeOrderBtn.isHidden = true
                
                noDataView.isHidden = false
                cartTbl.isHidden = true
            }
        }
    }
    
    func didFinish(with status: AIPaymentStatus, response: [String : Any]) {
        print("🔶 Paytm Callback Response: ", response)
    }
    
    private let appInvoke = AIHandler()

    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        placeOrderHeight.constant = 0
        placeOrderTop.constant = 0
        placeOrderBottom.constant = 0
        placeOrderBtn.isHidden = true
        
        
        topView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = 40
        topView.clipsToBounds = true
        noDataView.isHidden =  true
        getCartItemList()
        // Do any additional setup after loading the view.
    }
   
    func getCartItemList() {
        let url = URLS.Consumer().cartList
        let parameter:Parameters = [
            "user_id":userProfile[userProfileKeys().id].stringValue
        ]
        
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
            print("json \(json)")
            if json["status"].boolValue == true {
                self.cartDataList = json["data"].arrayValue
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
        }
        
        
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func placeOrder(_ sender: UIButton) {
        
//        var orderid = ""
//
//        for item in cartDataList {
//            orderid.append("\(item["cart_item_id"].stringValue),")
//        }
//        orderid.removeLast()
//        let parameter:Parameters = [
//            "cart_id":orderid
//        ]
//        let url = URLS.Consumer().placeOrder
//        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
//            if json["status"].boolValue == true {
//                AppDelegate.sharedAppDelegate.goToRootView()
//            } else {
//                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
//
//                }
//            }
//        }

        var totalPrice:Int = 0
        for item in cartDataList {
            totalPrice += Int("\(item["amount"].stringValue)") ?? 0
        }
        print("totalPrice \(totalPrice)")
        let promocodeVC = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutStepFourPromocodeVC") as! CheckOutStepFourPromocodeVC
        promocodeVC.TotalPrice = "\(totalPrice)"
        promocodeVC.cartDataList = self.cartDataList
        self.navigationController?.pushViewController(promocodeVC, animated: true)
      
    }
    func openPaymentWebVC(_ controller: UIViewController?) {
            if let vc = controller {
                DispatchQueue.main.async {[weak self] in
                    self?.present(vc, animated: true, completion: nil)
                }
            }
        }
    @IBAction func removeItemBtn(_ sender: UIButton) {
        let url = URLS.Consumer().RemovecartList
        let parameter:Parameters = [
            "cartid":cartDataList[sender.tag]["cart_item_id"].stringValue
        ]
        
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
            if json["status"].boolValue == true {
                self.getCartItemList()
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
           
            
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ConsumerCardListVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartDataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cartListCell", for: indexPath) as! cartListCell
        
        cell.nameLbl.text = cartDataList[indexPath.row]["product_name"].stringValue
        cell.qtyLbl.text = cartDataList[indexPath.row]["quantity"].stringValue
        cell.supplierLbl.text = cartDataList[indexPath.row]["supplier_code"].stringValue
        
        cell.productImg.imageFromUrl(message: "", urlString: cartDataList[indexPath.row]["product_image"].stringValue) { staus in
        }
        
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: Double("\(cartDataList[indexPath.row]["amount"].stringValue)") as! NSNumber) {
//            cell.priceLbl.text = "\(formattedTipAmount)"
            cell.priceLbl.text = formattedTipAmount
        }
        
        cell.removeBtn.tag = indexPath.row
        return cell
    }
    
}

class cartListCell:UITableViewCell {
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var supplierLbl: UILabel!
    
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var qtyLbl: UILabel!
}
