//
//  ConsumerSubCategoryListVC.swift
//  KhanjiMark
//
//  Created by Apple on 15/06/21.
//

import UIKit
import SwiftyJSON
import Alamofire
class ConsumerSubCategoryListVC:UIViewController {
    
    @IBOutlet weak var subCategoryCollection: UICollectionView!
    @IBOutlet weak var topView: UIView!
    var SubcategoryList = [JSON]()
    var selectedCategoryID = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        topView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = 40
        topView.clipsToBounds = true
        getData()
        // Do any additional setup after loading the view.
    }
    
    func getData() {
        let parameter:Parameters = [
            "category_id":selectedCategoryID
        ]
        
        let url = URLS.Comman().getProductSubCategory
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
            if json["status"].boolValue == true {
                self.SubcategoryList = json["data"].arrayValue
                self.setCollectionView()
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
        }
        
    }
    
    func setCollectionView() {
        subCategoryCollection.delegate = self
        subCategoryCollection.dataSource = self
        subCategoryCollection.reloadData()
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ConsumerSubCategoryListVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SubcategoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCatCollectionCell", for: indexPath) as! ProductCatCollectionCell
        
        cell.productName.text = SubcategoryList[indexPath.row]["subcategory"].stringValue
        cell.productImg.imageFromUrl(message: "", urlString: SubcategoryList[indexPath.row]["image"].stringValue) { staus in
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        ConsumerProductListVC
        let productList = self.storyboard?.instantiateViewController(withIdentifier: "ConsumerProductListVC") as! ConsumerProductListVC
        productList.selectedSubCategoryID = SubcategoryList[indexPath.row]["id"].stringValue
        
        productList.selectedCategoryID = SubcategoryList[indexPath.row]["category_id"].stringValue
        self.navigationController?.pushViewController(productList, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/3.0
        
        return CGSize(width: width - 10, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 0, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    
}

