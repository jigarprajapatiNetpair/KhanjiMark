//
//  ConsumerTopProductCategoryListVC.swift
//  KhanjiMark
//
//  Created by Apple on 11/06/21.
//

import UIKit
import SwiftyJSON
import Alamofire
class ConsumerTopProductCategoryListVC: UIViewController {

    @IBOutlet weak var productCollection: UICollectionView!
    @IBOutlet weak var topView: UIView!
    var categoryList = [JSON]()
    override func viewDidLoad() {
        super.viewDidLoad()
        topView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = 40
        topView.clipsToBounds = true
        setCollectionView()
        // Do any additional setup after loading the view.
    }
    func setCollectionView() {
        productCollection.delegate = self
        productCollection.dataSource = self
        productCollection.reloadData()
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ConsumerTopProductCategoryListVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCatCollectionCell", for: indexPath) as! ProductCatCollectionCell
        print("categoryList \(categoryList)")
        cell.productName.text = categoryList[indexPath.row]["category"].stringValue
        cell.productImg.imageFromUrl(message: "", urlString: categoryList[indexPath.row]["thumbnail"].stringValue) { staus in
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let subCategoryVC = self.storyboard?.instantiateViewController(withIdentifier: "ConsumerSubCategoryListVC") as! ConsumerSubCategoryListVC
        subCategoryVC.selectedCategoryID = categoryList[indexPath.row]["id"].stringValue
        self.navigationController?.pushViewController(subCategoryVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/3.0
        
        return CGSize(width: width - 10, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 0, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    
}

