//
//  ConsumerProductListVC.swift
//  KhanjiMark
//
//  Created by Apple on 15/06/21.
//

import UIKit
import SwiftyJSON
import Alamofire
class ConsumerProductListVC:UIViewController {
    
    @IBOutlet weak var ProductCollection: UICollectionView!
    @IBOutlet weak var topView: UIView!
    var productList = [JSON]()
    var selectedCategoryID = ""
    var selectedSubCategoryID = ""
    
    var whereToCome = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        topView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = 40
        topView.clipsToBounds = true
        getData()
        // Do any additional setup after loading the view.
    }
    
    func getData() {
        var parameter:Parameters = [:]
        var url = ""
        if whereToCome == "TopSupplier" {
            parameter = [
                "supplier_id":selectedCategoryID
            ]
            url = URLS.Consumer().SupplierProductList
        } else {
            parameter = [
                "category_id":selectedCategoryID,
                "sub_category_id":selectedSubCategoryID
            ]
            url = URLS.Consumer().productList
        }
        
        
        
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
            if json["status"].boolValue == true {
                self.productList = json["data"].arrayValue
                self.setCollectionView()
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
        }
        
    }
    
    func setCollectionView() {
        ProductCollection.delegate = self
        ProductCollection.dataSource = self
        ProductCollection.reloadData()
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ConsumerProductListVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("productList \(productList[indexPath.row])")
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCatCollectionCell", for: indexPath) as! ProductCatCollectionCell
        print("SubcategoryList \(productList[indexPath.row])")
        cell.productName.text = productList[indexPath.row]["product_name"].stringValue
        cell.productImg.imageFromUrl(message: "", urlString: productList[indexPath.row]["image"].stringValue) { staus in
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let productdetails = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
//
        print("productList[indexPath.row] \(productList[indexPath.row])")
        if productList[indexPath.row]["product_id"].stringValue != "" {
            productdetails.selectedProductID = productList[indexPath.row]["product_id"].stringValue
        } else {
            productdetails.selectedProductID = productList[indexPath.row]["id"].stringValue
        }
        
       self.navigationController?.pushViewController(productdetails, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/3.0
        
        return CGSize(width: width - 10, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 0, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    
}

