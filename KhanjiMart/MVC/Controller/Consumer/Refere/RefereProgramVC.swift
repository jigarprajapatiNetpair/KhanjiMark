//
//  RefereProgramVC.swift
//  KhanjiMark
//
//  Created by RMV on 7/7/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import MIBlurPopup
class RefereProgramVC: UIViewController {

    @IBOutlet weak var earnPaymentLbl: UILabel!
    @IBOutlet weak var topView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        topView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = 40
        topView.clipsToBounds = true
        
        
                let string = NSMutableAttributedString(string: earnPaymentLbl.text!)
        string.setColorForText(textForAttribute: "Pay", withColor: #colorLiteral(red: 0.01960784314, green: 0.1803871095, blue: 0.4384189248, alpha: 1), font: UIFont(name: "SF UI Display Heavy", size: 28) ?? UIFont.boldSystemFont(ofSize: 24))
        string.setColorForText(textForAttribute: "tm", withColor: #colorLiteral(red: 0.02474007569, green: 0.7341817021, blue: 0.9469296336, alpha: 1), font: UIFont(name: "SF UI Display Heavy", size: 28) ?? UIFont.boldSystemFont(ofSize: 24))
        earnPaymentLbl.attributedText = string
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func uploadBtnTap(_ sender: UIButton) {
        let owenerdetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "RefereUploadPhotoBtn") as! RefereUploadPhotoBtn
        
        MIBlurPopup.show(owenerdetailsVC, on: self)
    }
    @IBAction func ownerDetailsBtnTap(_ sender: UIButton) {
        
        let owenerdetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ReferOwenerDetailsVC") as! ReferOwenerDetailsVC
        
        MIBlurPopup.show(owenerdetailsVC, on: self)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
