//
//  RefereUploadPhotoBtn.swift
//  KhanjiMark
//
//  Created by RMV on 7/8/21.
//

import UIKit

import MIBlurPopup
import Alamofire
import SwiftyJSON
class RefereUploadPhotoBtn: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate,MIBlurPopupDelegate {
    var popupView: UIView = UIView()
    
    var blurEffectStyle: UIBlurEffect.Style? = .none
    
    var initialScaleAmmount: CGFloat = 0.3
    
    var animationDuration: TimeInterval = 0.3
    
    @IBOutlet weak var phoneNumberTxt: TextField!
    @IBOutlet weak var cityNameTxt: TextField!
    @IBOutlet weak var imagePathTxt: TextField!
    @IBOutlet weak var selectedImgHeight: NSLayoutConstraint! //130
    @IBOutlet weak var selectedImg: UIImageView!
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        selectedImgHeight.constant = 0
        imagePicker.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func closeBtnTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func uploadImgBtn(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))

            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))

            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

            /*If you want work actionsheet on ipad
            then you have to use popoverPresentationController to present the actionsheet,
            otherwise app will crash on iPad */
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender
                alert.popoverPresentationController?.sourceRect = sender.bounds
                alert.popoverPresentationController?.permittedArrowDirections = .up
            default:
                break
            }

            self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
       if let pickedImage = info[.originalImage] as? UIImage {
        selectedImg.image = pickedImage
        selectedImgHeight.constant = 130
       }
        if let title = info[.imageURL] as? URL {
            imagePathTxt.text = title.absoluteString
        }
       picker.dismiss(animated: true, completion: nil)
   }
    
    
    @IBAction func submitBtn(_ sender: UIButton) {
        if Validation() {
            let uploadImgParameter:Parameters = [
                "refer_city_name":cityNameTxt.text ?? "",
                "customer_mobile_number":phoneNumberTxt.text ?? ""
            ]
            let imageUploadURL = URL(string: URLS.Consumer().referandEarneDetails)
            showActivityWithMessage(message: "", inView: self.view)
            let headers: HTTPHeaders = [
                    /* "Authorization": "your_access_token",  in case you need authorization header */
                    "Content-type": "multipart/form-data"
                ]
            AF.upload(multipartFormData: { multipartFormData in
                
                for (ker, value) in uploadImgParameter{
                    multipartFormData.append(((value as? String)?.data(using: .utf8))!, withName: ker)
                       }
                
                multipartFormData.append((self.selectedImg.image?.pngData())!, withName: "uploded_photo", fileName: "\(Date.init().timeIntervalSince1970).png", mimeType: "image/png")
            },
            to: imageUploadURL!, method: .post , headers: headers)
            .responseJSON(completionHandler: { (response) in
                hideActivity()
                print("image response \(response)")
                let json = JSON(response.data!)
                
                if json["status"].boolValue == true {
                    Toast.show(message: "Successfully sent details", controller: self) { status in
                        self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    Toast.show(message: json["message"].stringValue, controller: self) { status in
                        
                    }
                }
            })
        }
    }
    
    func Validation() ->Bool {
        if cityNameTxt.text == "" {
            Toast.show(message: "please enter city", controller: self) { status in
                self.cityNameTxt.becomeFirstResponder()
            }
            return false
        } else if phoneNumberTxt.text == "" {
            Toast.show(message: "please enter mobile number", controller: self) { status in
                self.phoneNumberTxt.becomeFirstResponder()
            }
            return false
        } else if selectedImg.image == nil {
            Toast.show(message: "please select Photo", controller: self) { status in
                
            }
            return false
        } else  {
            return true
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
