//
//  ReferOwenerDetailsVC.swift
//  KhanjiMark
//
//  Created by RMV on 7/8/21.
//

import UIKit
import MIBlurPopup
import Alamofire
class ReferOwenerDetailsVC: UIViewController,MIBlurPopupDelegate {
    var popupView: UIView = UIView()
    
    var blurEffectStyle: UIBlurEffect.Style? = .none
    
    var initialScaleAmmount: CGFloat = 0.3
    
    var animationDuration: TimeInterval = 0.3
    

    @IBOutlet weak var ownerAddress: UITextView!
    @IBOutlet weak var mobileNumber: TextField!
    @IBOutlet weak var ownerName: TextField!
    @IBOutlet weak var OwnercontactNumber: TextField!
    @IBOutlet weak var cityTxt: TextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func submitBtnTap(_ sender: UIButton) {
        if Validation() {
            let parameter:Parameters = [
                "refer_city_name": cityTxt.text ?? "",
                "owner_contact_number": OwnercontactNumber.text ?? "",
                "owner_name": ownerName.text ?? "",
                "owner_address": ownerAddress.text ?? "",
                "customer_mobile_number": mobileNumber.text ?? ""
                
            ]
            
            let url = URLS.Consumer().referandEarneDetails
            
            NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
                if json["status"].boolValue == true {
                    Toast.show(message: "Successfully sent details", controller: self) { status in
                        self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    Toast.show(message: json["message"].stringValue, controller: self) { status in
                        
                    }
                }
            }
            
            
        }
    }
    func Validation() ->Bool {
        if cityTxt.text == "" {
            Toast.show(message: "please enter city", controller: self) { status in
                self.cityTxt.becomeFirstResponder()
            }
            return false
        } else if OwnercontactNumber.text == ""{
            Toast.show(message: "please enter ower contact number", controller: self) { status in
                self.OwnercontactNumber.becomeFirstResponder()
            }
            return false
        } else if ownerName.text == "" {
            Toast.show(message: "please enter ower name", controller: self) { status in
                self.OwnercontactNumber.becomeFirstResponder()
            }
            return false
        } else if ownerAddress.text == "" {
            Toast.show(message: "please enter ower address", controller: self) { status in
                self.OwnercontactNumber.becomeFirstResponder()
            }
            return false
        } else if mobileNumber.text == "" {
            Toast.show(message: "please enter mobile number", controller: self) { status in
                self.OwnercontactNumber.becomeFirstResponder()
            }
            return false
        } else  {
            return true
        }
    }
    @IBAction func closeBtn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
