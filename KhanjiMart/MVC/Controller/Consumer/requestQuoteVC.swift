//
//  requestQuoteVC.swift
//  KhanjiMark
//
//  Created by Apple on 12/06/21.
//

import UIKit
import Alamofire
import SwiftyJSON
class requestQuoteVC: UIViewController {

    @IBOutlet weak var addressTxt: UITextView!
    @IBOutlet weak var sometingTextTxt: UITextView!
    @IBOutlet weak var numbeTxt: TextField!
    @IBOutlet weak var emailaddressTxt: TextField!
    @IBOutlet weak var nameTxt: TextField!
    @IBOutlet weak var topView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        topView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = 40
        topView.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func submitBtnTap(_ sender: UIButton) {
        if validation() {
            
            let parameter:Parameters = [
             "visitor_name":nameTxt.text ?? "",
                "visitor_email":emailaddressTxt.text ?? "",
                "visitor_phone":numbeTxt.text ?? "",
                "visitor_message":sometingTextTxt.text ?? "",
                "visitor_address":addressTxt.text ?? "",
            ]
            let url = URLS.Consumer().requestQuot
            NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { (json) in
                if json["status"].boolValue == true {
                    Toast.show(message: json["message"].stringValue, controller: self) { status in
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    Toast.show(message: json["message"].stringValue, controller: self) { status in
                        print("")
                    }
                }
            }
        }
    }
    
    func validation() -> Bool {
            
        
        if nameTxt.text == "" {
            Toast.show(message: "Please enter name", controller: self) { status in
                self.nameTxt.becomeFirstResponder()
            }
            return false
        }  else if emailaddressTxt.text == "" {
            Toast.show(message: "Please email address", controller: self) { status in
                self.emailaddressTxt.becomeFirstResponder()
            }
            return false
        }  else if numbeTxt.text == "" {
            Toast.show(message: "Please mobile number", controller: self) { status in
                self.numbeTxt.becomeFirstResponder()
            }
            return false
        }  else if sometingTextTxt.text == "" {
            Toast.show(message: "Please enter something text", controller: self) { status in
                self.sometingTextTxt.becomeFirstResponder()
            }
            return false
        }  else if addressTxt.text == "" {
            Toast.show(message: "Please enter expected address", controller: self) { status in
                self.addressTxt.becomeFirstResponder()
            }
            return false
        }  else {
            return true
        }
        
        
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
