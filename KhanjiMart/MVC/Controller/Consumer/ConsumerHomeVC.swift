//
//  ConsumerHomeVC.swift
//  KhanjiMark
//
//  Created by Apple on 10/06/21.
//

import UIKit
import Alamofire
import SwiftyJSON
class ConsumerHomeVC: UIViewController,SWRevealViewControllerDelegate, UITableViewDataSource,UITableViewDelegate, TableViewInsideCollectionViewDelegate {
    func cellTaped(data: JSON) {
        print("JSON \(data)")
        
            let productdetails = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        productdetails.selectedProductID = data["id"].stringValue
           self.navigationController?.pushViewController(productdetails, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TopProductCategoryJson.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopProductTableCell", for: indexPath) as! TopProductTableCell
        let item = TopProductCategoryJson[indexPath.row]
        cell.ProductTitle.text = item["category"].stringValue
        cell.ProductList = item["products"].arrayValue
        
        cell.delegate = self

        return cell
    }
    
    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        if position.rawValue == 3 {
            if isShopeEnable {
                isShopeEnable = false
                let productCategory = self.storyboard?.instantiateViewController(withIdentifier: "ConsumerTopProductCategoryListVC") as! ConsumerTopProductCategoryListVC
                productCategory.categoryList = ProductCategoryJson
                
                self.navigationController?.pushViewController(productCategory, animated: true)
            }
            if isSuppliersEnable {
                isSuppliersEnable = false
                let supplierList = self.storyboard?.instantiateViewController(withIdentifier: "ConsumerTopSupplierVC") as! ConsumerTopSupplierVC
                supplierList.supplierList = TopSupplierJson
                
                self.navigationController?.pushViewController(supplierList, animated: true)
            }
            if isOrderListEnable {
                isOrderListEnable = false
                let orderList = self.storyboard?.instantiateViewController(withIdentifier: "SupplierOrderReceiverdVC") as!  SupplierOrderReceiverdVC
                self.navigationController?.pushViewController(orderList, animated: true)
            }
        }
    }
    
    
    @IBOutlet weak var topPRoductListHeight: NSLayoutConstraint!
    @IBOutlet weak var topProductListTbl: UITableView!
    @IBOutlet weak var topSupplierCollection: UICollectionView!
    @IBOutlet weak var regerProgramImg: UIImageView!
    @IBOutlet weak var requestImg: UIImageView!
//    @IBOutlet weak var topProductCategoryCollection: UICollectionView!
    @IBOutlet weak var productCategoryCollection: UICollectionView!
    @IBOutlet weak var bannerCollection: UICollectionView!
    @IBOutlet weak var pageController: UIPageControl!
    var BannerImageJson = [JSON]()
    var ProductCategoryJson = [JSON]()
    var TopProductCategoryJson = [JSON]()
    var TopSupplierJson = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topPRoductListHeight.constant = 0
        getOfferBannerData()
        getProductData()
        getTopProdutct()
        getTopSupplier()
        requestImg.imageFromUrl(message: "", urlString: "http://snapcodeinfotech.com/demo/khanijmart/uploads/request.png") { (status) in
            
        }
        regerProgramImg.imageFromUrl(message: "", urlString: "http://snapcodeinfotech.com/demo/khanijmart/uploads/refer.jpg") { (status) in
            
        }
        self.revealViewController()?.rearViewRevealWidth = self.view.frame.width / 1.2
        self.revealViewController().rearViewRevealOverdraw = 20
        self.revealViewController()?.rightViewRevealWidth = UIScreen.main.bounds.width
        
        // Do any additional setup after loading the view.
    }   
    
    @IBAction func MenuBtnTap(_ sender: UIButton) {
        self.revealViewController()?.delegate = self
        self.revealViewController().revealToggle(self)
    }
    
    
    @IBAction func CartBtnTap(_ sender: UIButton) {
//        ConsumerCardListVC
        let cartList = self.storyboard?.instantiateViewController(withIdentifier: "ConsumerCardListVC") as! ConsumerCardListVC
        self.navigationController?.pushViewController(cartList, animated: true)
        
    }
    
    @IBAction func profileBtnTap(_ sender: UIButton) {
//        SupplierProfileVC
        let cunsomerProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "SupplierProfileVC") as! SupplierProfileVC
        self.navigationController?.pushViewController(cunsomerProfileVC, animated: true)

    }
    
    @IBAction func searchBtnTap(_ sender: UIButton) {
        //        ComsumerSearchVC
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "ComsumerSearchVC") as! ComsumerSearchVC
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    @IBAction func productViewAllBtnTap(_ sender: UIButton) {
        //        ConsumerTopProductCategoryListVC
        let productCategory = self.storyboard?.instantiateViewController(withIdentifier: "ConsumerTopProductCategoryListVC") as! ConsumerTopProductCategoryListVC
        productCategory.categoryList = ProductCategoryJson
        
        self.navigationController?.pushViewController(productCategory, animated: true)
    }
    
    
    @IBAction func reuestQuateBtnTap(_ sender: UIButton) {
        //        requestQuoteVC
        let quoteVC = self.storyboard?.instantiateViewController(withIdentifier: "requestQuoteVC") as! requestQuoteVC
        self.navigationController?.pushViewController(quoteVC, animated: true)
    }
    
    @IBAction func refereProgramBtnTap(_ sender: UIButton) {
//        RefereProgramVC
        
        
        let referProgramer = self.storyboard?.instantiateViewController(withIdentifier: "RefereProgramVC") as! RefereProgramVC
        self.navigationController?.pushViewController(referProgramer, animated: true)
    }
    
    @IBAction func supplierViewAllBtn(_ sender: UIButton) {
        
        let supplierList = self.storyboard?.instantiateViewController(withIdentifier: "ConsumerTopSupplierVC") as! ConsumerTopSupplierVC
        supplierList.supplierList = TopSupplierJson
        
        self.navigationController?.pushViewController(supplierList, animated: true)
    }
    
    @IBAction func supplierProductBtn(_ sender: UIButton) {
        let productListVC = self.storyboard?.instantiateViewController(withIdentifier: "ConsumerProductListVC") as! ConsumerProductListVC
        productListVC.whereToCome = "TopSupplier"
        productListVC.selectedCategoryID = TopSupplierJson[sender.tag]["id"].stringValue
        self.navigationController?.pushViewController(productListVC, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension ConsumerHomeVC {
    func getOfferBannerData() {
        
        NetworkManager().getDataFromServer(url: URLS.Consumer().OfferBanner , method: .get, parameter: [:], view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.BannerImageJson = json["data"].arrayValue
                self.getupCollectionView()
            } else {
                Toast.show(message: json["message"].stringValue, controller: self) { status in
                    print("")
                }
            }
        }
    }
    
    func getProductData() {
        
        NetworkManager().getDataFromServer(url: URLS.Comman().getProductCategory , method: .get, parameter: [:], view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.ProductCategoryJson = json["data"].arrayValue
                self.getupProductCollectionView()
            } else {
                Toast.show(message: json["message"].stringValue, controller: self) { status in
                    print("")
                }
            }
        }
    }
    
    
    func getTopProdutct() {
        NetworkManager().getDataFromServer(url: URLS.Consumer().TopProductCategory , method: .get, parameter: [:], view: self.view) { json in
            
            if json["status"].boolValue == true {
                
                let topProduct = json["data"].arrayValue
                for item in topProduct {
                    if item["products"].arrayValue.count > 0 {
                        self.TopProductCategoryJson.append(item)
                    }
                }

                self.topPRoductListHeight.constant = CGFloat(self.TopProductCategoryJson.count * 212)
                self.getupTopProductCollectionView()
            } else {
                Toast.show(message: json["message"].stringValue, controller: self) { status in
                    print("")
                }
            }
        }
    }
    
    func getTopSupplier() {
        NetworkManager().getDataFromServer(url: URLS.Consumer().TopsupplierList , method: .get, parameter: [:], view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.TopSupplierJson = json["data"].arrayValue
                self.getupTopSupplierCollectionView()
            } else {
                Toast.show(message: json["message"].stringValue, controller: self) { status in
                    print("")
                }
            }
        }
    }
    
    
}
extension ConsumerHomeVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = self.bannerCollection.contentOffset.x / self.bannerCollection.frame.size.width
        self.pageController.currentPage = Int(index)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch  collectionView{
        case bannerCollection:
            return BannerImageJson.count
        case productCategoryCollection:
            return ProductCategoryJson.count
        case topSupplierCollection:
            return TopSupplierJson.count
        default:
            return 0
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        switch  collectionView{
        case bannerCollection:
            let offerbannr = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionCell", for: indexPath) as! BannerCollectionCell
            offerbannr.bannerImg.imageFromUrl(message: "", urlString: BannerImageJson[indexPath.row].stringValue) { status in
            }
            cell = offerbannr
        case productCategoryCollection:
            let product = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCatCollectionCell", for: indexPath) as! ProductCatCollectionCell
            product.productName.text = ProductCategoryJson[indexPath.row]["category"].stringValue
            product.productImg.imageFromUrl(message: "", urlString: ProductCategoryJson[indexPath.row]["thumbnail"].stringValue) { staus in
                
            }
            cell = product
//        case topProductCategoryCollection:
//            let Topproduct = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCatCollectionCell", for: indexPath) as! ProductCatCollectionCell
//            Topproduct.productName.text = TopProductCategoryJson[indexPath.row]["product_name"].stringValue
//            Topproduct.productImg.imageFromUrl(message: "", urlString: TopProductCategoryJson[indexPath.row]["product_image"].stringValue) { staus in
//
//            }
//            cell = Topproduct
//
        case topSupplierCollection:
            let TopSupplier = collectionView.dequeueReusableCell(withReuseIdentifier: "TopSupplierCollectionCell", for: indexPath) as! TopSupplierCollectionCell
            TopSupplier.supplierCode.text = TopSupplierJson[indexPath.row]["supplier_code"].stringValue
            TopSupplier.supplierImg.imageFromUrl(message: "", urlString: TopSupplierJson[indexPath.row]["profile_photo"].stringValue) { staus in
            }
            TopSupplier.supplierProductBtn.tag = indexPath.row
            TopSupplier.supplierCount.text = TopSupplierJson[indexPath.row]["count"].stringValue
            cell = TopSupplier
        default:
            break
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView {
        case productCategoryCollection:
            let subCategoryVC = self.storyboard?.instantiateViewController(withIdentifier: "ConsumerSubCategoryListVC") as! ConsumerSubCategoryListVC
            subCategoryVC.selectedCategoryID = ProductCategoryJson[indexPath.row]["id"].stringValue
            self.navigationController?.pushViewController(subCategoryVC, animated: true)
        
        default:
            break
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch  collectionView{
        case bannerCollection:
            return CGSize(width: self.view.bounds.width, height: 160)
      case productCategoryCollection:
            return CGSize(width: 130, height: 150)
        case topSupplierCollection:
            return CGSize(width: 160, height: 190)
        default:
            return CGSize(width: 130, height: 150)
        }
        
    }
    
    func getupCollectionView() {
        self.bannerCollection.dataSource = self
        self.bannerCollection.delegate = self
        self.pageController.numberOfPages = BannerImageJson.count
        self.pageController.currentPage = 0
        self.bannerCollection.reloadData()
    }
    
    func getupProductCollectionView() {
        self.productCategoryCollection.dataSource = self
        self.productCategoryCollection.delegate = self
        self.productCategoryCollection.reloadData()
    }
    
    func getupTopProductCollectionView() {
        self.topProductListTbl.dataSource = self
        self.topProductListTbl.delegate = self
        self.topProductListTbl.reloadData()
    }
    
    func getupTopSupplierCollectionView() {
        self.topSupplierCollection.dataSource = self
        self.topSupplierCollection.delegate = self
        self.topSupplierCollection.reloadData()
    }
    
}
class BannerCollectionCell:UICollectionViewCell {
    
    @IBOutlet weak var bannerImg: UIImageView!
}

class ProductCatCollectionCell:UICollectionViewCell {
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImg: UIImageView!

    @IBOutlet weak var productPriceLbl: NSLayoutConstraint!
    @IBOutlet weak var productPrice: UILabel!
}

class TopSupplierCollectionCell:UICollectionViewCell {
    @IBOutlet weak var ViewImgBtn: UIButton!
    @IBOutlet weak var supplierImg: UIImageView!
    @IBOutlet weak var supplierProductBtn: UIButton!
    @IBOutlet weak var supplierCount: UILabel!
    @IBOutlet weak var supplierCode: UILabel!
}

class TopProductTableCell:UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ProductList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let Topproduct = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCatCollectionCell", for: indexPath) as! ProductCatCollectionCell
        Topproduct.productName.text = ProductList[indexPath.row]["product_name"].stringValue
        Topproduct.productImg.imageFromUrl(message: "", urlString: ProductList[indexPath.row]["product_image"].stringValue) { staus in

        }
        
        if ProductList[indexPath.row]["SmallestPrice"].stringValue == "0" || ProductList[indexPath.row]["SmallestPrice"].stringValue == "" {
            Topproduct.productPrice.isHidden = true
            Topproduct.productPrice.text = ""
            Topproduct.productPriceLbl.constant = 0
        } else {
            Topproduct.productPrice.text = "Starting from  ₹\(ProductList[indexPath.row]["SmallestPrice"].stringValue)/\(ProductList[indexPath.row]["unit"].stringValue)"
            Topproduct.productPrice.isHidden = false
            Topproduct.productPriceLbl.constant = 10
        }
         return Topproduct
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        delegate?.cellTaped(data: ProductList[indexPath.row])
    }
    
    weak var delegate:TableViewInsideCollectionViewDelegate?

    var selectedIndex = -1
    var ProductList = [JSON]() {
        didSet {
            if ProductList.count > 0 {
                ProductCollectionsView.delegate = self
                ProductCollectionsView.dataSource = self
                ProductCollectionsView.reloadData()
            }
        }
    }
    @IBOutlet weak var ProductCollectionsView: UICollectionView!
    @IBOutlet weak var ProductTitle: UILabel!
    override  func awakeFromNib() {
        ProductCollectionsView.delegate = self
        ProductCollectionsView.dataSource = self
    }
}
