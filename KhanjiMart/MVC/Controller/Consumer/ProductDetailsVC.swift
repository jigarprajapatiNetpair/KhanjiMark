//
//  ProductDetailsVC.swift
//  KhanjiMark
//
//  Created by RMV on 7/11/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import Shuffle_iOS
class ProductDetailsVC: UIViewController  {

  
    @IBOutlet weak var productImgView: SwipeCardStack!
    var productPhotos:[String] = [] {
        didSet {
            productImgView.dataSource = self
            productImgView.delegate = self
            productImgView.reloadData()
        }
    }
    @IBOutlet weak var priceType: UILabel!
    @IBOutlet weak var descriptionTv: UITextView!
    @IBOutlet weak var productName: UILabel!
    var selectedProductID = ""
    var AllProductDetails = JSON()
    @IBOutlet weak var topView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        topView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = CGFloat(Double(topView.frame.height / 2))
        topView.clipsToBounds = true
        setData()

        // Do any additional setup after loading the view.
    }
    func setData() {
        
        let url = URLS.Consumer().productDetails
        let parameter:Parameters = [
            "product_id":selectedProductID
        ]
        
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
            if json["status"].boolValue == true {
                self.AllProductDetails = json["data"].self
                self.productPhotos = (self.AllProductDetails["product_images"].object as? [String])!

                print("all images \(self.productPhotos)")
                self.productName.text = self.AllProductDetails["product_name"].stringValue
                
                let htmlString = self.AllProductDetails["payment_description"].stringValue
                let data = (htmlString.data(using: .utf8)!)
                
                let attributedString = try? NSAttributedString(
                    data: data,
                    options: [.documentType: NSAttributedString.DocumentType.html],
                    documentAttributes: nil)
                
                
                self.priceType.attributedText = attributedString
                
                let String = self.AllProductDetails["description"].stringValue
                let dataString = (String.data(using: .utf8)!)
                
                let attributed = try? NSAttributedString(
                    data: dataString,
                    options: [.documentType: NSAttributedString.DocumentType.html],
                    documentAttributes: nil)
                self.descriptionTv.attributedText = attributed
                
                
            } else {
               
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
            
        }
        
        
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buyNowBtn(_ sender: UIButton) {
        
        let checkOut = self.storyboard?.instantiateViewController(withIdentifier: "CheckOutStepFirstVC") as! CheckOutStepFirstVC
        checkOut.selectedProduct = AllProductDetails
        self.navigationController?.pushViewController(checkOut, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProductDetailsVC: SwipeCardStackDataSource, SwipeCardStackDelegate {
   
  func cardStack(_ cardStack: SwipeCardStack, cardForIndexAt index: Int) -> SwipeCard {
    let card = SwipeCard()
    card.footerHeight = 10
    card.swipeDirections = [.left, .up, .right]
    for direction in card.swipeDirections {
      card.setOverlay(TinderCardOverlay(direction: direction), forDirection: direction)
    }
    let model = productPhotos[index]
    card.content = TinderCardContentView(withImage: model)
    card.footer = TinderCardFooterView(withTitle: "", subtitle: "")

    return card
  }

  func numberOfCards(in cardStack: SwipeCardStack) -> Int {
    return productPhotos.count
  }

  func didSwipeAllCards(_ cardStack: SwipeCardStack) {
    print("Swiped all cards!")
    productImgView.reloadData()
  }

  func cardStack(_ cardStack: SwipeCardStack, didUndoCardAt index: Int, from direction: SwipeDirection) {
//    print("Undo \(direction) swipe on \(cardModels[index].name)")
  }

  func cardStack(_ cardStack: SwipeCardStack, didSwipeCardAt index: Int, with direction: SwipeDirection) {
//    print("Swiped \(direction) on \(cardModels[index].name)")
  }

  func cardStack(_ cardStack: SwipeCardStack, didSelectCardAt index: Int) {
    print("Card tapped")
  }

}
