//
//  SupplierOrderReceivedCancelVC.swift
//  KhanjiMark
//
//  Created by RMV on 8/29/21.
//

import UIKit

import DropDown
import SwiftyJSON
import Alamofire
class ConsumerOrderReceivedCancelVC: UIViewController, UIGestureRecognizerDelegate {
    var detailsjson = JSON()
    
    @IBOutlet weak var subView: UIView!

    @IBOutlet weak var reasonTxt: UITextView!
    var tapGestures = UITapGestureRecognizer()
    var SubtapGestures = UITapGestureRecognizer()
    override func viewDidLoad() {
        super.viewDidLoad()
        tapGestures.delegate = self
        tapGestures = UITapGestureRecognizer(target: self, action: #selector(viewdismiss))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGestures)
        SubtapGestures = UITapGestureRecognizer(target: self, action: #selector(viewdismiss))
        self.subView.addGestureRecognizer(SubtapGestures)
        self.subView.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
    }
    
    @objc func viewdismiss(recognizer:UIGestureRecognizer) {
        if recognizer.state == .ended {
            if recognizer != SubtapGestures {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func updateOrderBtn(_ sender: UIButton) {
        cancelOrder()
    }
    
    @IBAction func cancelBtnTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func cancelOrder() {
        let parameter:Parameters = [
            "order_id":detailsjson["id"].stringValue,
            "reason_text":reasonTxt.text ?? "",
            "customer_id":userProfile[userProfileKeys().id].stringValue,
        ]
        print("parameter \(parameter)")
        let url = URLS.Consumer().cancelOrder
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
            print("json \(json)")
            if json["status"].boolValue == true {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
