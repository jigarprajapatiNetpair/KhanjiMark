//
//  SupplierProfileVC.swift
//  KhanjiMark
//
//  Created by Apple on 02/06/21.
//

import UIKit
import DropDown
import SwiftyJSON
import Alamofire
class SupplierProfileVC: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    
    @IBOutlet weak var editProfileBtn: UIButton!
    @IBOutlet weak var backBtnTap: UIButton!
    //    *******************************************************
//    All text field
    
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var officeView: UIView!
    @IBOutlet weak var officeHeight: NSLayoutConstraint! // 388
    @IBOutlet weak var bankHeight: NSLayoutConstraint! // 673
    @IBOutlet weak var bankView: UIView!
    
    // personal info fields
    
    @IBOutlet weak var companyNameTxt: TextField!
    @IBOutlet weak var firstNameTxt: TextField!
    @IBOutlet weak var mrmsLbl: UILabel!
    @IBOutlet weak var middleNameTxt: TextField!
    @IBOutlet weak var lastNameTxt: TextField!
    @IBOutlet weak var mrMSDropDown: UIButton!
    
    
    
    // contact indo fields
    
    @IBOutlet weak var emailAddressTxt: TextField!
    @IBOutlet weak var mobileTxt: TextField!
    
    // Address Fields
    @IBOutlet weak var addressOneTxt: TextField!
    @IBOutlet weak var addressTwoTxt: TextField!
    @IBOutlet weak var siteDistrictLbl: UILabel!
    @IBOutlet weak var siteCityLbl: UILabel!
    @IBOutlet weak var sitePincodeLbl: UILabel!
    
    @IBOutlet weak var siteDistrictOneDropBtn: UIButton!
    @IBOutlet weak var siteTakuloOneDropBtn: UIButton!
    @IBOutlet weak var sitePinCodeOneDropBtn: UIButton!
    
    
    @IBOutlet weak var OfficeDistrictOneDropBtn: UIButton!
    @IBOutlet weak var OfficeTakuloOneDropBtn: UIButton!
    @IBOutlet weak var OfficePinCodeOneDropBtn: UIButton!
    
    
    @IBOutlet weak var officeAddressOneTxt: TextField!
    @IBOutlet weak var officeAddressTwoTxt: TextField!
    
    @IBOutlet weak var officeDistrictLbl: UILabel!
    @IBOutlet weak var officeCityLbl: UILabel!
    @IBOutlet weak var officePincodeLbl: UILabel!
    
    // Bank Information
    
    @IBOutlet weak var bankNameTxt: TextField!
    @IBOutlet weak var bankAccountNumTxt: TextField!
    @IBOutlet weak var bankAccountNameTxt: TextField!
    @IBOutlet weak var bankIFSCNumberTxt: TextField!
    @IBOutlet weak var panNumTxt: TextField!
    @IBOutlet weak var GSTNumTxt: TextField!
    
    
    
    //    *******************************************************
    
    
    
    let MrMsArray:[String] = ["Mr","Mrs","Ms"]
    let dropdown = DropDown()
    
    
    var SiteDistrictString:[String] = []
    let siteDistrictOneDrop = DropDown()
    var selectedSiteDistricut = ""
    
    var SiteTakulaString:[String] = []
    let siteTakuloOneDrop = DropDown()
    var selectedSiteTaluka = ""
    
    var SitePinCodeString:[String] = []
    let sitePinCodeOneDrop = DropDown()
    var selectedSitePin = ""
    
    
    var OfficeDistrictOneString:[String] = []
    let OfficeDistrictOneDrop = DropDown()
    var selectedOfficeDistricut = ""
    
    
    
    var OfficeTakuloOneString:[String] = []
    let OfficeTakuloOneDrop = DropDown()
    var selectedOfficeTaluka = ""
    
    var OfficePinCodeOneString:[String] = []
    let OfficePinCodeOneDrop = DropDown()
    var selectedOfficePin = ""
    
    
    var SiteDistrictJSon = [JSON]()
    var SitetakulaJSon = [JSON]()
    var SitePinCodeJSon = [JSON]()
    
    
    var OfficeDistrictJSon = [JSON]()
    var OfficetakulaJSon = [JSON]()
    var OfficePinCodeJSon = [JSON]()
    
    
    
    @IBOutlet weak var userProfileImg: UIImageView!
    @IBOutlet weak var topView: UIView!
    
    
    var imagePicker = UIImagePickerController()
    var whereToCome = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        imagePicker.delegate = self
        setupUI()
        setupData()
        // Do any additional setup after loading the view.
    }
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func editProfileBtn(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))

            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))

            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

            /*If you want work actionsheet on ipad
            then you have to use popoverPresentationController to present the actionsheet,
            otherwise app will crash on iPad */
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender
                alert.popoverPresentationController?.sourceRect = sender.bounds
                alert.popoverPresentationController?.permittedArrowDirections = .up
            default:
                break
            }

            self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
       if let pickedImage = info[.originalImage] as? UIImage {
        userProfileImg.image = pickedImage
       }
       picker.dismiss(animated: true, completion: nil)
   }
    
    
    @IBAction func saveBtn(_ sender: UIButton) {
        if validation() {
            var parameter:Parameters = [:]
            var url = ""
            var imageUploadURL = ""
            if userProfile[userProfileKeys().userType].stringValue == UserType().Supplier {
                parameter = [
                "company_name":companyNameTxt.text ?? "",
                "title":mrmsLbl.text ?? "",
                "first_name":firstNameTxt.text ?? "",
                "middle_name":middleNameTxt.text ?? "",
                "last_name":lastNameTxt.text ?? "",
                "email":emailAddressTxt.text ?? "",
                "mobile_number":mobileTxt.text ?? "",
                "site_address_1":addressOneTxt.text ?? "",
                "site_address_2":addressTwoTxt.text ?? "",
                "site_address_distict":selectedSiteDistricut,
                "site_address_taluka":selectedSiteTaluka,
                "site_address_pincode":selectedSitePin,
                "office_address_1":officeAddressOneTxt.text ?? "",
                "office_address_2":officeAddressTwoTxt.text ?? "",
                "office_address_distict":selectedOfficeDistricut,
                "office_address_taluka":selectedOfficeTaluka,
                "office_address_pincode":selectedOfficePin,
                "bank_name":bankNameTxt.text ?? "",
                "bank_account_numner":bankAccountNumTxt.text ?? "",
                "account_name":bankAccountNameTxt.text ??  "" ,
                "bank_ifsc_code":bankIFSCNumberTxt.text ?? "",
                "pan_number":panNumTxt.text ?? "",
                "gst_number":GSTNumTxt.text ?? "",
                "user_id":userProfile[userProfileKeys().id].stringValue
                ]
                url = URLS.SellerSide().profileUpdate
                imageUploadURL =  URLS.SellerSide().profileImageUpload
            } else {
                parameter = [
                "company_name":companyNameTxt.text ?? "",
                "title":mrmsLbl.text ?? "",
                "first_name":firstNameTxt.text ?? "",
                "middle_name":middleNameTxt.text ?? "",
                "last_name":lastNameTxt.text ?? "",
                "email":emailAddressTxt.text ?? "",
                "mobile_number":mobileTxt.text ?? "",
                "address_1":addressOneTxt.text ?? "",
                "address_2":addressTwoTxt.text ?? "",
                "district":selectedSiteDistricut,
                "taluka":selectedSiteTaluka,
                "pincode":selectedSitePin,
                "user_id":userProfile[userProfileKeys().id].stringValue
                ]
                url = URLS.Consumer().supplierProfileDetails
                imageUploadURL = URLS.Consumer().supplierProfileDetails
            }
            
            
            let uploadImgParameter = ["user_id":userProfile[userProfileKeys().id].stringValue]
            let imageUpload = URL(string: imageUploadURL)
            showActivityWithMessage(message: "", inView: self.view)
            let headers: HTTPHeaders = [
                    /* "Authorization": "your_access_token",  in case you need authorization header */
                    "Content-type": "multipart/form-data"
                ]
            AF.upload(multipartFormData: { multipartFormData in
                
                for (ker, value) in uploadImgParameter{
                    multipartFormData.append((value).data(using: .utf8)!, withName: ker)
                       }
                
                multipartFormData.append((self.userProfileImg.image?.jpegData(compressionQuality: 1))!, withName: "profile_photo", fileName: "\(Date.init().timeIntervalSince1970).png", mimeType: "image/jpg")
            },
            to: imageUpload!, method: .post , headers: headers)
            .responseJSON(completionHandler: { (response) in
                hideActivity()
                print("image response \(response)")
                let json = JSON(response.data!)
                
                if json["status"].boolValue == true {
                    print("json \(json)")
                }
            })
            
            
            NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
                if json["status"].boolValue == true {
                    Toast.show(message: "Profile update successfull", controller: self) { (status) in
                        let parameter:Parameters = [
                            "username":userProfile[userProfileKeys().mobile_number].stringValue,
                            "password":userProfile[userProfileKeys().password].stringValue,
                            "user_type":userProfile[userProfileKeys().userType].stringValue,
                        ]
                       
                        NetworkManager().getDataFromServer(url: URLS.Comman().login, method: .post, parameter: parameter, view: self.view) { json in
                            if json["status"].boolValue == true {
                                   let userJson = json["data"].self
                                   addUserJSONDataToUserDefaults(userData: userJson)
                                   userProfile = getCachedUserJSONData()!
                            }
                            
                        }
                    }
                } else {
                    Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                        
                    }
                }
            }
            
            
            
        }
    }
    
    
    @IBAction func MrMsDropDownBtnTap(_ sender: UIButton) {
        dropdown.show()
    }
    
    
    @IBAction func SiteDictrictDropDownBtnTap(_ sender: UIButton) {
        siteDistrictOneDrop.show()
    }
    
    @IBAction func SiteCityDropDownBtnTap(_ sender: UIButton) {
        siteTakuloOneDrop.show()
    }
    
    @IBAction func SitePinCodeDropDownBtnTap(_ sender: UIButton) {
        sitePinCodeOneDrop.show()
    }

    
    
    @IBAction func OfficeDictrictDropDownBtnTap(_ sender: UIButton) {
        OfficeDistrictOneDrop.show()
    }
    
    @IBAction func OfficeCityDropDownBtnTap(_ sender: UIButton) {
        OfficeTakuloOneDrop.show()
    }
    
    @IBAction func OfficePinCodeDropDownBtnTap(_ sender: UIButton) {
        OfficePinCodeOneDrop.show()
    }
   
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


//  All funcations
extension SupplierProfileVC {
    
func setupUI() {
    if whereToCome == "home" || userProfile[userProfileKeys().userType].stringValue == UserType().Customer
    {
        self.backBtnTap.isHidden = false
        
    } else {
        self.backBtnTap.isHidden = true
    }
    
    
    if userProfile[userProfileKeys().userType].stringValue == UserType().Supplier {
    selectedSiteDistricut = userProfile[userProfileKeys().site_address_distict].stringValue
    selectedSiteTaluka = userProfile[userProfileKeys().site_address_taluka].stringValue
    selectedSitePin = userProfile[userProfileKeys().siteAddressPincode].stringValue
    } else  {
        selectedSiteDistricut = userProfile[userProfileKeys().district].stringValue
        selectedSiteTaluka = userProfile[userProfileKeys().taluka].stringValue
        selectedSitePin = userProfile[userProfileKeys().pincode].stringValue
    }
    selectedOfficeDistricut = userProfile[userProfileKeys().office_address_distict].stringValue
    selectedOfficeTaluka = userProfile[userProfileKeys().office_address_taluka].stringValue
    selectedOfficePin = userProfile[userProfileKeys().office_address_pincode].stringValue
    
    
    userProfileImg.cornerRadius = 45
    userProfileImg.clipsToBounds = true
    topView.layer.maskedCorners = [.layerMaxXMaxYCorner]
    topView.layer.cornerRadius = 60
    topView.clipsToBounds = true
    setupDropDown()
    getDataFromAPI()
}
    
    func setupData() {
        
        if userProfile[userProfileKeys().userType].stringValue == UserType().Supplier {
            bankHeight.constant = 674
            officeHeight.constant = 388
            addressLbl.text = "Site Address"
            officeView.isHidden = false
            bankView.isHidden = false
        } else {
            bankHeight.constant = 0
            officeHeight.constant = 0
            officeView.isHidden = true
            bankView.isHidden = true
            addressLbl.text = "Address"
        }
        
        userProfileImg.imageFromUrl(message: "", urlString: userProfile[userProfileKeys().profilePhoto].stringValue) { (status) in
            
        }
        companyNameTxt.text = userProfile[userProfileKeys().company_name].stringValue
        firstNameTxt.text = userProfile[userProfileKeys().first_name].stringValue
        mrmsLbl.text = userProfile[userProfileKeys().title].stringValue
        middleNameTxt.text = userProfile[userProfileKeys().middle_name].stringValue
        lastNameTxt.text = userProfile[userProfileKeys().last_name].stringValue
        
        
        emailAddressTxt.text = userProfile[userProfileKeys().email].stringValue
        mobileTxt.text = userProfile[userProfileKeys().mobile_number].stringValue
        if userProfile[userProfileKeys().userType].stringValue == UserType().Supplier {
        addressOneTxt.text = userProfile[userProfileKeys().site_address_1].stringValue
        addressTwoTxt.text = userProfile[userProfileKeys().site_address_2].stringValue
        } else {
            addressOneTxt.text = userProfile[userProfileKeys().address_1].stringValue
            addressTwoTxt.text = userProfile[userProfileKeys().address_2].stringValue
        }
        officeAddressOneTxt.text = userProfile[userProfileKeys().office_address_1].stringValue
        officeAddressTwoTxt.text = userProfile[userProfileKeys().office_address_2].stringValue
        
        
        bankNameTxt.text = userProfile[userProfileKeys().bank_name].stringValue
        bankAccountNumTxt.text = userProfile[userProfileKeys().bankAccountNumner].stringValue
        bankIFSCNumberTxt.text = userProfile[userProfileKeys().bank_ifsc_code].stringValue
        bankAccountNameTxt.text = userProfile[userProfileKeys().bank_account_name].stringValue
        
        panNumTxt.text = userProfile[userProfileKeys().pan_number].stringValue
        GSTNumTxt.text = userProfile[userProfileKeys().gstNumber].stringValue
        
        
    }
    
    
    func setupDropDown() {
        dropdown.anchorView = mrMSDropDown // UIView or UIBarButtonItem
        dropdown.dataSource = MrMsArray
        dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            mrmsLbl.text = item
            dropdown.hide()
        }
        
        SetSiteDistrictDropDown()
        SetSiteTalukaDropDown()
        SetSitePinDropDown()
        
        
        SetOfficeDistrictDropDown()
        SetOfficeTalukaDropDown()
        SetOfficePinDropDown()
        
    }
    
    
    func SetSiteDistrictDropDown() {
        
        siteDistrictOneDrop.anchorView = siteDistrictOneDropBtn  // UIView or UIBarButtonItem
        siteDistrictOneDrop.dataSource = SiteDistrictString
        siteDistrictOneDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            siteDistrictLbl.text = item
            siteCityLbl.text = "select taluko"
            sitePincodeLbl.text = "select pin code"
            selectedSiteDistricut = SiteDistrictJSon[index]["id"].stringValue
            getSiteTalukaData()
            siteDistrictOneDrop.hide()
        }
        
    }
    
    func SetSiteTalukaDropDown() {
        siteTakuloOneDrop.anchorView = siteTakuloOneDropBtn  // UIView or UIBarButtonItem
        siteTakuloOneDrop.dataSource = SiteTakulaString
        siteTakuloOneDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            siteCityLbl.text = item
            sitePincodeLbl.text = "select pin code"
            selectedSiteTaluka = SitetakulaJSon[index]["id"].stringValue
            getSitePinCodeData()
            siteTakuloOneDrop.hide()
        }
        
    }
    
    func SetSitePinDropDown() {
        sitePinCodeOneDrop.anchorView =   sitePinCodeOneDropBtn// UIView or UIBarButtonItem
        sitePinCodeOneDrop.dataSource = SitePinCodeString
        sitePinCodeOneDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            sitePincodeLbl.text = item
            selectedSitePin = SitePinCodeJSon[index]["id"].stringValue
            sitePinCodeOneDrop.hide()
        }
        
    }
    

    
    func SetOfficeDistrictDropDown() {
        OfficeDistrictOneDrop.anchorView = OfficeDistrictOneDropBtn // UIView or UIBarButtonItem
        OfficeDistrictOneDrop.dataSource = OfficeDistrictOneString
        OfficeDistrictOneDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            officeDistrictLbl.text = item
            officeCityLbl.text = "select taluko"
            officePincodeLbl.text = "select pin code"
            selectedOfficeDistricut = OfficeDistrictJSon[index]["id"].stringValue
            self.getOfficeTalukaData()
            OfficeDistrictOneDrop.hide()
        }
    }
    
    func SetOfficeTalukaDropDown() {
        OfficeTakuloOneDrop.anchorView = OfficeTakuloOneDropBtn // UIView or UIBarButtonItem
        OfficeTakuloOneDrop.dataSource = OfficeTakuloOneString
        OfficeTakuloOneDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            officeCityLbl.text = item
            officePincodeLbl.text = "select pin code"
            selectedOfficeTaluka = OfficetakulaJSon[index]["id"].stringValue
            getOfficePinCodeData()
            OfficeTakuloOneDrop.hide()
        }
        
    }
    
    func SetOfficePinDropDown() {
        OfficePinCodeOneDrop.anchorView = OfficePinCodeOneDropBtn // UIView or UIBarButtonItem
        OfficePinCodeOneDrop.dataSource = OfficePinCodeOneString
        OfficePinCodeOneDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            officePincodeLbl.text = item
            selectedOfficePin = OfficePinCodeJSon[index]["id"].stringValue
            OfficePinCodeOneDrop.hide()
        }
    }
    
    
    func getDataFromAPI() {
        let siteDistrictParameter = [
            "state_id":"5"
        ]
        
        NetworkManager().getDataFromServer(url: URLS.Comman().GetDistrict, method: .post, parameter: siteDistrictParameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.SiteDistrictJSon = json["data"].arrayValue
                self.OfficeDistrictJSon = json["data"].arrayValue
                self.SiteDistrictString.removeAll()
                self.OfficeDistrictOneString.removeAll()
                for item in self.SiteDistrictJSon {
                    if self.selectedSiteDistricut == item["id"].stringValue {
                        self.siteDistrictLbl.text = item["district"].stringValue
                        self.selectedSiteDistricut = item["id"].stringValue
                    }
                    
                    if self.selectedOfficeDistricut == item["id"].stringValue {
                        self.officeDistrictLbl.text = item["district"].stringValue
                        self.selectedOfficeDistricut = item["id"].stringValue
                    }
                    
                    self.SiteDistrictString.append(item["district"].stringValue)
                    self.OfficeDistrictOneString.append(item["district"].stringValue)
                }
                
                if userProfile[userProfileKeys().userType].stringValue == UserType().Supplier {
                    self.SetSiteDistrictDropDown()
                    self.SetOfficeDistrictDropDown()
                    
                    self.getSiteTalukaData()
                    self.getOfficeTalukaData()
                } else {
                    
                    self.SetSiteDistrictDropDown()
                    self.getSiteTalukaData()
                }
                
                
                
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
            
        }
        
        
    }
    
    func getSiteTalukaData() {
        
            let siteTalukaParameter = [
                "district_id":self.selectedSiteDistricut
            ]
            
            NetworkManager().getDataFromServer(url: URLS.Comman().GetTaluka, method: .post, parameter: siteTalukaParameter, view: self.view) { json in
                
                if json["status"].boolValue == true {
                    self.SitetakulaJSon = json["data"].arrayValue
                    self.SiteTakulaString.removeAll()
                    for item in self.SitetakulaJSon {
                        self.SiteTakulaString.append(item["taluka"].stringValue)
                        
                        if self.selectedSiteTaluka == item["id"].stringValue {
                            self.siteCityLbl.text = item["taluka"].stringValue
                            self.selectedSiteTaluka = item["id"].stringValue
                        }
                        
                    }
                    self.SetSiteTalukaDropDown()
                    self.getSitePinCodeData()
                    
                } else {
                    Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                        
                    }
                }
                
            }
        
    }
    
    func getSitePinCodeData() {
        let sitePinCodeParameter = [
            "district_id":self.selectedSiteDistricut,
            "taluka_id":self.selectedSiteTaluka
        ]
        
        NetworkManager().getDataFromServer(url: URLS.Comman().GetPincode, method: .post, parameter: sitePinCodeParameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.SitePinCodeJSon = json["data"].arrayValue
                self.SitePinCodeString.removeAll()
               
                for item in self.SitePinCodeJSon {
                    self.SitePinCodeString.append("\(item["name"].stringValue)(\(item["pincode"].stringValue))")
                    if userProfile[userProfileKeys().userType].stringValue == UserType().Supplier {
                        if self.selectedSitePin == item["id"].stringValue {
                            self.sitePincodeLbl.text = "\(item["name"].stringValue)(\(item["pincode"].stringValue))"
                            self.selectedSitePin  = item["id"].stringValue
                        }
                    } else {
                      if self.selectedSitePin == item["pincode"].stringValue {
                            self.sitePincodeLbl.text = "\(item["name"].stringValue)(\(item["pincode"].stringValue))"
                            self.selectedSitePin  = item["id"].stringValue
                        }
                    }
                }
                self.SetSitePinDropDown()
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
            
        }
        
    }
    
    
    
    func getOfficeTalukaData() {
        let officeTalukaParameter = [
            "district_id":self.selectedOfficeDistricut
        ]
        
        NetworkManager().getDataFromServer(url: URLS.Comman().GetTaluka, method: .post, parameter: officeTalukaParameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.OfficetakulaJSon = json["data"].arrayValue
                self.OfficeTakuloOneString.removeAll()
                for item in self.OfficetakulaJSon {
                    self.OfficeTakuloOneString.append(item["taluka"].stringValue)
                    if self.selectedOfficeTaluka == item["id"].stringValue {
                        self.officeCityLbl.text = item["taluka"].stringValue
                        self.selectedOfficeTaluka = item["id"].stringValue
                    }
                }
                self.SetOfficeTalukaDropDown()
                self.getOfficePinCodeData()
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
            
        }
    }
    
    
    func getOfficePinCodeData() {
        let sitePinCodeParameter = [
            "district_id":self.selectedOfficeDistricut,
            "taluka_id":self.selectedOfficeTaluka
        ]
        
        NetworkManager().getDataFromServer(url: URLS.Comman().GetPincode, method: .post, parameter: sitePinCodeParameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.OfficePinCodeJSon = json["data"].arrayValue
                self.OfficePinCodeOneString.removeAll()
                for item in self.OfficePinCodeJSon {
                    self.OfficePinCodeOneString.append("\(item["name"].stringValue)(\(item["pincode"].stringValue)")
                    if self.selectedOfficePin == item["id"].stringValue {
                        self.officePincodeLbl.text = "\(item["name"].stringValue)(\(item["pincode"].stringValue)"
                        self.selectedOfficePin = item["id"].stringValue
                    }
                }
                
                self.SetOfficePinDropDown()
                
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
            
        }
    }
    
    func validation() -> Bool {
        
//        if companyNameTxt.text == "" {
//            Toast.show(message: "Please enter company name", controller: self) { status in
//
//            }
//            return false
//        } else
        if firstNameTxt.text == "" {
            Toast.show(message: "Please enter first name", controller: self) { status in
                
            }
            return false
        } else if middleNameTxt.text == "" {
            Toast.show(message: "Please enter middle name", controller: self) { status in
                
            }
            return false
        }  else if lastNameTxt.text == "" {
            Toast.show(message: "Please enter last name", controller: self) { status in
                
            }
            return false
        }  else if emailAddressTxt.text == "" {
            Toast.show(message: "Please enter email address", controller: self) { status in
                
            }
            return false
        }  else if mobileTxt.text == "" {
            Toast.show(message: "Please enter mobile number", controller: self) { status in
                
            }
            return false
        }
        
        else if siteDistrictLbl.text == "" {
           Toast.show(message: "Selete Site District name", controller: self) { status in
               
           }
           return false
       }
        
        
        else if siteCityLbl.text == "" {
           Toast.show(message: "Selete Site Taluka name", controller: self) { status in
               
           }
           return false
       }
        
        else if sitePincodeLbl.text == "" {
           Toast.show(message: "Selete Site Pin Code", controller: self) { status in
               
           }
           return false
       }
        
        
        else if officeDistrictLbl.text == "" {
           Toast.show(message: "Selete Office District name", controller: self) { status in
               
           }
           return false
       }
        
        
        else if officeCityLbl.text == "" {
           Toast.show(message: "Selete Office Taluka name", controller: self) { status in
               
           }
           return false
       }
        
        else if officePincodeLbl.text == "" {
           Toast.show(message: "Selete Office Pin Code", controller: self) { status in
               
           }
           return false
       }
        
        
        else if bankNameTxt.text == "" && userProfile[userProfileKeys().userType].stringValue == UserType().Supplier {
            Toast.show(message: "Please enter bank name", controller: self) { status in
                
            }
            return false
        }   else if bankAccountNumTxt.text == "" && userProfile[userProfileKeys().userType].stringValue == UserType().Supplier {
            Toast.show(message: "Please enter account number", controller: self) { status in
                
            }
            return false
        }   else if bankNameTxt.text == "" && userProfile[userProfileKeys().userType].stringValue == UserType().Supplier {
            Toast.show(message: "Please enter bank account name", controller: self) { status in
                
            }
            return false
        }   else if bankIFSCNumberTxt.text == ""  && userProfile[userProfileKeys().userType].stringValue == UserType().Supplier {
            Toast.show(message: "Please enter IFSC code", controller: self) { status in
                
            }
            return false
        } else {
            return true
        }
        
        
       
        
    }
}
 
