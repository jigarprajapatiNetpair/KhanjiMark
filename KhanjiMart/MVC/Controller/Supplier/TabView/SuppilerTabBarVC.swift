//
//  SuppilerTabBarVC.swift
//  KhanjiMark
//
//  Created by Apple on 02/06/21.
//

import UIKit
import SOTabBar

class SuppilerTabBarVC: SOTabBarController, UIViewControllerTransitioningDelegate, SOTabBarControllerDelegate {
    func tabBarController(_ tabBarController: SOTabBarController, didSelect viewController: UIViewController) {
        
        switch viewController.view.tag {
        case 0:
            break
        case 4:
            print("where to come")
            break
        default:
            break
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.transitioningDelegate = self
        self.delegate = self
        self.selectedIndex = 0
        let HomeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SuppilerHomeVC")
        
        let SearchVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SupplierSearchVC")
        
        let AddProduct = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SupplierAddProductVC")
//
        let NotificationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SupplierNotificationVC")
//
        let ProfileVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SupplierProfileVC")
//
        HomeVC.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "home-Tab"), selectedImage: UIImage(named: "home-s"))
//
        SearchVC.tabBarItem = UITabBarItem(title: "Search", image: UIImage(named: "serch"), selectedImage: #imageLiteral(resourceName: "serch-s"))
        
        AddProduct.tabBarItem = UITabBarItem(title: "Add", image: UIImage(named: "add"), selectedImage: #imageLiteral(resourceName: "add-s"))
//
        NotificationVC.tabBarItem = UITabBarItem(title: "Notification", image: UIImage(named: "bell"), selectedImage: #imageLiteral(resourceName: "bell-s"))
//
        ProfileVC.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "user"), selectedImage: #imageLiteral(resourceName: "user-s"))
//
//
        
        
        viewControllers = [HomeVC,SearchVC,AddProduct,NotificationVC,ProfileVC]

        
//        SOTabBarSetting.tabBarSizeSelectedImage = CGFloat(80)
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
