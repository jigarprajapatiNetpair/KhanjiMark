//
//  SuppilerHomeVC.swift
//  KhanjiMark
//
//  Created by Apple on 02/06/21.
//

import UIKit
import Alamofire
import BBWebImage
import PMAlertController
class SuppilerHomeVC: UIViewController {

    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var userAddress: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var userProfileBanner: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUserData()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func logOutBtnTap(_ sender: UIButton) {
        let alertVC = PMAlertController(title: "Logout", description: "Are you sure!!", image: nil, style: .alert)

        alertVC.addAction(PMAlertAction(title: "Cancel", style: .cancel, action: { () -> Void in
                    print("Capture action Cancel")
                }))

        alertVC.addAction(PMAlertAction(title: "YES", style: .default, action: { () in
            print("Capture action ok")
                    UserDefaultsManager().isLogIn = false
            AppDelegate.sharedAppDelegate.goToRootView()
                }))

        self.present(alertVC, animated: true, completion: nil)
    }
    func setUserData() {
        
        userName.text = "\(userProfile[userProfileKeys().first_name].stringValue) \(userProfile[userProfileKeys().last_name].stringValue)".uppercased()
        
        userAddress.text = "\(userProfile[userProfileKeys().office_address_1].stringValue) \(userProfile[userProfileKeys().office_address_2].stringValue)"
        
        
        print("userProfile \(userProfile[userProfileKeys().profilePhoto].stringValue)")
        userProfileImage.imageFromUrl(message: "", urlString: userProfile[userProfileKeys().profilePhoto].stringValue) { (status) in
            
        }
        userProfileBanner.imageFromUrl(message: "", urlString: userProfile[userProfileKeys().profilePhoto].stringValue) { (status) in
            
        }
        userProfileImage.layer.cornerRadius = userProfileImage.frame.height / 2
    }
    
    
    @IBAction func walletBtnTap(_ sender: UIButton) {
        let walletHistory = self.storyboard?.instantiateViewController(withIdentifier: "PaymentHistoryVC") as! PaymentHistoryVC
        self.navigationController?.pushViewController(walletHistory, animated: true)
        
    }
    
    
    @IBAction func profileBntTap(_ sender: UIButton) {
        
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "SupplierProfileVC") as! SupplierProfileVC
        profileVC.whereToCome = "home"
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @IBAction func CartBntTap(_ sender: UIButton) {
//        SupplierProductListVC
        
        let productList = self.storyboard?.instantiateViewController(withIdentifier: "SupplierProductListVC") as! SupplierProductListVC
        self.navigationController?.pushViewController(productList, animated: true)
    }
    
    @IBAction func OrderRecBntTap(_ sender: UIButton) {
        let reveiceOrder = self.storyboard?.instantiateViewController(withIdentifier: "SupplierOrderReceiverdVC") as! SupplierOrderReceiverdVC
        self.navigationController?.pushViewController(reveiceOrder, animated: true)
    }
    
    @IBAction func SettingBntTap(_ sender: UIButton) {
        let changePasswordVC = self.storyboard?.instantiateViewController(withIdentifier: "SupplierChangePasswordVC") as! SupplierChangePasswordVC
        changePasswordVC.modalPresentationStyle = .overFullScreen
        changePasswordVC.modalTransitionStyle = .crossDissolve
        self.present(changePasswordVC, animated: true, completion: nil)
        
//        self.navigationController?.pushViewController(changePasswordVC, animated: true)
    }
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
