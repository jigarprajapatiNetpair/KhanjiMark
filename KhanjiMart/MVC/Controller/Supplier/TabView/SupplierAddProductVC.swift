//
//  SupplierAddProductVC.swift
//  KhanjiMark
//
//  Created by Apple on 03/06/21.
//

import UIKit
import SwiftyJSON
import Alamofire
import DropDown

import YPImagePicker
class SupplierAddProductVC: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate,YPImagePickerDelegate {
    func noPhotos() {
        print("no photo")
    }
    
    
    func shouldAddToSelection(indexPath: IndexPath, numSelections: Int) -> Bool {
        return true
    }
    
    var imagePicker = YPImagePicker()
    
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var pinCodeCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var pinCodeCollection: UICollectionView!
    @IBOutlet weak var DistrictLbl: UILabel!
    @IBOutlet weak var CityLbl: UILabel!
    @IBOutlet weak var PincodeLbl: UILabel!
    @IBOutlet weak var DistrictOneDropBtn: UIButton!
    @IBOutlet weak var TakuloOneDropBtn: UIButton!
    @IBOutlet weak var PinCodeOneDropBtn: UIButton!
    
    @IBOutlet weak var ProductListTbl: UITableView!
    
    var DistrictString:[String] = []
    let DistrictOneDrop = DropDown()
    var selectedDistricut = ""
    
    var TakulaString:[String] = []
    let TakuloOneDrop = DropDown()
    var selectedTaluka = ""
    
    var PinCodeString:[String] = []
    let PinCodeOneDrop = DropDown()
    var selectedPinNames:[String] = []
    var selectedPinArrayId:[String] = []
    
    var DistrictJSon = [JSON]()
    var takulaJSon = [JSON]()
    var PinCodeJSon = [JSON]()
    
    
    var mainCategoryJson:[JSON] = []
    let mainCategoryeDrop = DropDown()
    var mainCategoryString:[String] = []
    
    
    var subCategoryJson:[JSON] = []
    let subCategoryDrop = DropDown()
    var subCategoryString:[String] = []
    
    
    var productJson:[JSON] = []
    let productDrop = DropDown()
    var productNameString:[String] = []
    
    
    var selectdImagePickerTag = -1
    var productListCount = 1
    var productDetails = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()

        pinCodeCollectionHeight.constant = 0
        setupUI()
        DistrictLbl.text = "select district"
        CityLbl.text = "select Taluko"
        PincodeLbl.text = "select pin code"
        getMainCategory()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addProductBtnTap(_ sender: UIButton) {
        
//        print("productListCount \(productListCount)")
        var details = [String:Any]()
//        print("indexpath \(IndexPath(row: productListCount, section: 0))")
        let cell:addProductList = ProductListTbl.cellForRow(at: IndexPath(row: productListCount - 1, section: 0)) as! addProductList
        details["product_category_id"] = cell.selectedMainCategoryId
        
        details["product_sub_category_id"] = cell.selectedSubCategoryId
        
        details["product_id"] = cell.selectedProductId
        
        details["product_special_price"] = cell.specialPriceTxt.text ?? ""
        
        details["min_order_qty"] = cell.priceTxt.text ?? ""
        
        details["in_stock"] = cell.selectedStockValue
//        details["suppliers_product_image"] = cell.productImg.image?.jpegData(compressionQuality: 1)
        details["suppliers_product_image"] = cell.SelectedImage

print("details \(details)")
        productDetails.append(details)
        productListCount += 1
        ProductListTbl.reloadData()
    }
    
    @IBAction func DictrictDropDownBtnTap(_ sender: UIButton) {
        DistrictOneDrop.show()
    }
    
    @IBAction func CityDropDownBtnTap(_ sender: UIButton) {
        if takulaJSon.isEmpty {
            Toast.show(message: "Selecte first district", controller: self) { status in
                self.DistrictOneDrop.show()
            }
        } else {
            TakuloOneDrop.show()
        }
        
    }
    
    @IBAction func PinCodeDropDownBtnTap(_ sender: UIButton) {
        if takulaJSon.isEmpty {
            Toast.show(message: "Selecte first district", controller: self) { status in
                self.DistrictOneDrop.show()
            }
        }
        else  if PinCodeJSon.isEmpty {
            Toast.show(message: "Selecte first taluko", controller: self) { status in
                self.TakuloOneDrop.show()
            }
        } else {
            PinCodeOneDrop.show()
        }
        
    }
    
    @IBAction func deleteProductBth(_ sender: UIButton) {
        selectedPinNames.remove(at: sender.tag)
        selectedPinArrayId.remove(at: sender.tag)
        pinCodeCollection.reloadData()
    }
    
    @IBAction func imagePickerBtnTap(_ sender: UIButton) {
        selectdImagePickerTag = sender.tag
        let cell:addProductList = ProductListTbl.cellForRow(at: IndexPath(row: selectdImagePickerTag, section: 0)) as! addProductList
//        imagePicker
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        if cell.SelectedImage.count < 5 {
            self.present(alert, animated: true, completion: nil)
        }
        
        
        
        
        
       

    }
    
    func openCamera()
    {
        let cell:addProductList = ProductListTbl.cellForRow(at: IndexPath(row: selectdImagePickerTag, section: 0)) as! addProductList
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            if cell.SelectedImage.count < 5 {
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        

    }
    
    func openGallary()
    {
//        let imagePicker = UIImagePickerController()
//        imagePicker.delegate = self
//        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
//        imagePicker.allowsEditing = true
//
//        self.present(imagePicker, animated: true, completion: nil)
        
        
        
        var config = YPImagePickerConfiguration()
        config.isScrollToChangeModesEnabled = true
        config.onlySquareImagesFromCamera = true
        config.usesFrontCamera = false
        config.shouldSaveNewPicturesToAlbum = true
        config.albumName = "DefaultYPImagePickerAlbumName"
        config.startOnScreen = YPPickerScreen.library
        config.screens = [.library]
        config.showsCrop = .none
        config.targetImageSize = YPImageSize.original
        config.maxCameraZoomFactor = 1.0
//        config.preSelectItemOnMultipleSelection = true
        let cell:addProductList = ProductListTbl.cellForRow(at: IndexPath(row: selectdImagePickerTag, section: 0)) as! addProductList
        
        config.library.maxNumberOfItems = 5 - cell.SelectedImage.count
        config.wordings.libraryTitle = "Gallery"
        config.wordings.cameraTitle = "Camera"
        config.wordings.next = "OK"
        imagePicker = YPImagePicker(configuration: config)
        imagePicker.delegate = self
        imagePicker.didFinishPicking { [unowned imagePicker] items, _ in
            
            imagePicker.dismiss(animated: true) {
                print("in cancelled items \(items.count)")
                let cell:addProductList = self.ProductListTbl.cellForRow(at: IndexPath(row: self.selectdImagePickerTag, section: 0)) as! addProductList
                for item in items {
                    switch item {
                    case .photo(let photo):
                        cell.SelectedImage.append(photo.image)

                        print(photo)
                    case .video(let video):
                        print(video)
                    }
                }
//                cell.awakeFromNib()
                
                
                print("cell.SelectedImage \(cell.SelectedImage.count)")
                self.ProductListTbl.reloadData()
            }
//            imagePicker.dismiss(animated: true, completion: nil)
        }
        
        
//        imagePicker.didFinishPicking { [unowned imagePicker] items, cancelled in
//            if cancelled {
//                print("Picker was canceled")
//            }
//            imagePicker.dismiss(animated: true, completion: nil)
//        }
        if cell.SelectedImage.count < 5 {
            present(imagePicker, animated: true, completion: nil)
        }
        

    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.editedImage] as? UIImage {
            let cell:addProductList = ProductListTbl.cellForRow(at: IndexPath(row: selectdImagePickerTag, section: 0)) as! addProductList
            print("cell image tap")
            cell.SelectedImage.append(pickedImage)
            cell.selectedImgCollection.reloadData()
//            cell.awakeFromNib()
//            cell.productImg.image = pickedImage
//            cell.productImageHeight.constant = 140
            ProductListTbl.reloadData()
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func mainCatBtnTap(_ sender: UIButton) {
        let cell:addProductList = ProductListTbl.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! addProductList
        
        mainCategoryeDrop.anchorView = cell.mainCategoryBtn// UIView or UIBarButtonItem
        mainCategoryeDrop.dataSource = mainCategoryString
        cell.mainCategoryJson = mainCategoryJson
        mainCategoryeDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            cell.selectedMainCategoryId = cell.mainCategoryJson[index]["id"].stringValue
            cell.MainCategoryLbl.text = item
            cell.subCategoryLbl.text = "select sub category"
            self.subCategoryJson.removeAll()
            getSubCategory(selected: sender.tag)
        }
        mainCategoryeDrop.show()
    }
    
    
    @IBAction func subCateBtnTap(_ sender: UIButton) {
        let cell:addProductList = ProductListTbl.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! addProductList
        if cell.mainCategoryJson.count > 0 && subCategoryJson.count > 0 {
            subCategoryDrop.anchorView = cell.subCategoryBtn// UIView or UIBarButtonItem
            subCategoryDrop.dataSource = cell.subCategoryString
            
            subCategoryDrop.selectionAction = { [unowned self] (index: Int, item: String) in
                print("count \(cell.subCategoryJson.count)")
                print("index \(index)")
                cell.selectedSubCategoryId = cell.subCategoryJson[index]["id"].stringValue
                cell.subCategoryLbl.text = item
                cell.productJson.removeAll()
                cell.productLbl.text = "select product"
                self.productJson.removeAll()
                getProductList(selected: sender.tag)
            }
            subCategoryDrop.show()
        } else {
            cell.subCategoryLbl.text = "select sub category"
        }
        
    }
    
    @IBAction func productBtnTap(_ sender: UIButton) {
        let cell:addProductList = ProductListTbl.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! addProductList
        
        if cell.subCategoryJson.count > 0 && productJson.count > 0 {
            productDrop.anchorView = cell.productBtn// UIView or UIBarButtonItem
            productDrop.dataSource = cell.productNameString
            
            productDrop.selectionAction = { [unowned self] (index: Int, item: String) in
                cell.selectedProductId = cell.productJson[index]["id"].stringValue
                cell.productLbl.text = item
            }
            productDrop.show()
        } else {
            cell.productLbl.text = "select product"
        }
        
    }
    
    @IBAction func deleteBtnTap(_ sender: UIButton) {
        productListCount -= 1
        if productDetails.count > 0 {
            productDetails.removeLast()
        }
        
        ProductListTbl.reloadData()
    }
    
    func getMainCategory() {
        
        NetworkManager().getDataFromServer(url: URLS.Comman().getProductCategory, method: .get, parameter: [:], view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.mainCategoryJson = json["data"].arrayValue
                
                self.mainCategoryString.removeAll()
                for item in self.mainCategoryJson {
                    self.mainCategoryString.append(item["category"].stringValue)
                }
                
            } else {
                Toast.show(message: "Category not found", controller: self) { status in
                    
                }
            }
            
        }
        
    }
    
    func getSubCategory(selected:Int) {
        let cell:addProductList = ProductListTbl.cellForRow(at: IndexPath(row: selected, section: 0)) as! addProductList
        print("cell.selectedMainCategoryId \(cell.selectedMainCategoryId)")
        let parameter = [
            "category_id":cell.selectedMainCategoryId
        ]
        NetworkManager().getDataFromServer(url: URLS.Comman().getProductSubCategory, method: .post, parameter: parameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.subCategoryJson = json["data"].arrayValue
                cell.subCategoryJson = json["data"].arrayValue
                self.subCategoryString.removeAll()
                cell.subCategoryString.removeAll()
                for item in self.subCategoryJson {
                    self.subCategoryString.append(item["subcategory"].stringValue)
                    cell.subCategoryString.append(item["subcategory"].stringValue)
                }
            } else {
                Toast.show(message: "Sub category not found", controller: self) { status in
                    
                }
            }
            
        }
        
    }
    
    
    func getProductList(selected:Int) {
        let cell:addProductList = ProductListTbl.cellForRow(at: IndexPath(row: selected, section: 0)) as! addProductList
        print("cell.selectedMainCategoryId \(cell.selectedMainCategoryId)")
        print("cell.selectedSubCategoryId \(cell.selectedSubCategoryId)")
        let parameter = [
            "category_id":cell.selectedMainCategoryId,
            "sub_category_id":cell.selectedSubCategoryId
        ]
        NetworkManager().getDataFromServer(url: URLS.Consumer().productList, method: .post, parameter: parameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.productJson = json["data"].arrayValue
                cell.productJson = json["data"].arrayValue
                self.productNameString.removeAll()
                cell.productNameString.removeAll()
                for item in self.productJson {
                    self.productNameString.append(item["product_name"].stringValue)
                    cell.productNameString.append(item["product_name"].stringValue)
                }
                
            } else {
                Toast.show(message: "Product data not found", controller: self) { status in
                    
                }
            }
            
        }
        
    }
    
    
    @IBAction func saveBtnTap(_ sender: UIButton) {
        var productPrameter:Parameters = [:]
        
        let cell:addProductList = ProductListTbl.cellForRow(at: IndexPath(row: productListCount - 1, section: 0)) as! addProductList
        var details = [String:Any]()
        details["product_category_id"] = cell.selectedMainCategoryId
        
        details["product_sub_category_id"] = cell.selectedSubCategoryId
        
        details["product_id"] = cell.selectedProductId
        
        details["product_special_price"] = cell.specialPriceTxt.text ?? ""
        
        details["min_order_qty"] = cell.priceTxt.text ?? ""
        
        details["in_stock"] = cell.selectedStockValue
//        details["suppliers_product_image"] = cell.productImg.image?.jpegData(compressionQuality: 1)
        details["suppliers_product_image"] = cell.SelectedImage
//        print("details \(details)")
        productDetails.append(details)
        productPrameter = [
            "product_district_id": self.selectedDistricut,
            "product_taluka_id": self.selectedTaluka,
            "user_id": userProfile[userProfileKeys().id].stringValue,

        ]
        for item in 0 ..< selectedPinArrayId.count {
            productPrameter["product_pincode[\(item)]"] = selectedPinArrayId[item]
        }
        
        for detailsIndex in 0 ..< productDetails.count {
            let details = productDetails[detailsIndex]
            productPrameter["product_details[\(detailsIndex)][product_category]"] = details["product_category_id"]
            productPrameter["product_details[\(detailsIndex)][product_sub_category]"] = details["product_sub_category_id"]
            productPrameter["product_details[\(detailsIndex)][product_special_price]"] = details["product_special_price"]
            productPrameter["product_details[\(detailsIndex)][product_name]"] = details["product_id"]
            productPrameter["product_details[\(detailsIndex)][min_order_qty]"] = details["min_order_qty"]
            productPrameter["product_details[\(detailsIndex)][in_stock]"] = details["in_stock"]

        }
        
        print("productPrameter \(productPrameter)")
        let url = URLS.SellerSide().addProductList
        var count = -1
        showActivityWithMessage(message: "", inView: self.view)
        AF.upload(multipartFormData: { multipartFormData in
            
            for (key, value) in productPrameter{
                count += 1
                
                if let stringArray = value as? String {
                    // obj is a string array. Do something with stringArray
                    multipartFormData.append((stringArray as! String).data(using: .utf8)!, withName: key)
                } else if let Vimage  = value as? [UIImage] {
                    var Icount = -1

                    for item in Vimage {
                        Icount += 1
                        multipartFormData.append(item as! Data, withName: "product_details[\(count)][suppliers_product_image][\(Icount)]", fileName: "\(Date.init().timeIntervalSince1970).png", mimeType: "image/png")
                    }
                    
                }
//                else {
//                    multipartFormData.append(value as! Data, withName: "product_details[\(count)][suppliers_product_image]", fileName: "\(Date.init().timeIntervalSince1970).png", mimeType: "image/png")
//                }
               
            }
            
        },
        to: url as! URLConvertible, method: .post , headers: nil)
        .responseString(completionHandler: { (response) in
            hideActivity()
            print("response \(response)")
            let json = JSON(response.data!)
            print("json \(json)")
            if json["status"].boolValue == true {
                Toast.show(message: "Product update successfully", controller: self) { status in
                    AppDelegate.sharedAppDelegate.goToRootView()
                }
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    //
                }
            }
        })
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension SupplierAddProductVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productListCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addProductList", for: indexPath) as! addProductList
        
        cell.deleteBtn.tag = indexPath.row
        cell.productTitle.text = "Product \(indexPath.row + 1)"
        cell.photoPickerBtn.tag = indexPath.row
        cell.mainCategoryBtn.tag = indexPath.row
        cell.subCategoryBtn.tag = indexPath.row
        cell.productBtn.tag = indexPath.row
        return cell
    }
    
    
}
extension SupplierAddProductVC:UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedPinNames.count > 0 {
            pinCodeCollectionHeight.constant = 50
        } else {
            pinCodeCollectionHeight.constant = 0
        }
        return selectedPinNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pincodeTagCell", for: indexPath) as! pincodeTagCell
        cell.nameLbl.text = selectedPinNames[indexPath.row]
        cell.removePin.tag = indexPath.row
        return cell
    }
    
    
}

extension SupplierAddProductVC {
    func getDataFromAPI() {
        let siteDistrictParameter = [
            "state_id":"5"
        ]
        
        NetworkManager().getDataFromServer(url: URLS.Comman().GetDistrict, method: .post, parameter: siteDistrictParameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.DistrictJSon = json["data"].arrayValue
                
                self.DistrictString.removeAll()
                
                for item in self.DistrictJSon {
                    if self.selectedDistricut == item["id"].stringValue {
                        self.DistrictLbl.text = item["district"].stringValue
                        self.selectedDistricut = item["id"].stringValue
                    }
                    
                    self.DistrictString.append(item["district"].stringValue)
                    
                }
                self.SetDistrictDropDown()
                
                self.getTalukaData()
                
                
            } else {
                Toast.show(message: "District data not found", controller: self) { status in
                    
                }
            }
            
        }
        
        
    }
    
    func getTalukaData() {
        
        let siteTalukaParameter = [
            "district_id":self.selectedDistricut
        ]
        
        NetworkManager().getDataFromServer(url: URLS.Comman().GetTaluka, method: .post, parameter: siteTalukaParameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.takulaJSon = json["data"].arrayValue
                self.TakulaString.removeAll()
                for item in self.takulaJSon {
                    self.TakulaString.append(item["taluka"].stringValue)
                    
                    if self.selectedTaluka == item["id"].stringValue {
                        self.CityLbl.text = item["taluka"].stringValue
                        self.selectedTaluka = item["id"].stringValue
                    }
                    
                }
                self.SetTalukaDropDown()
                self.getPinCodeData()
            } else {
                
            }
            
        }
        
    }
    
    func getPinCodeData() {
        let sitePinCodeParameter = [
            "district_id":self.selectedDistricut,
            "taluka_id":self.selectedTaluka
        ]
        
        NetworkManager().getDataFromServer(url: URLS.Comman().GetPincode, method: .post, parameter: sitePinCodeParameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.PinCodeJSon = json["data"].arrayValue
                self.PinCodeString.removeAll()
                self.PinCodeString.insert("Select all", at: 0)
                for item in self.PinCodeJSon {
                    self.PinCodeString.append("\(item["name"].stringValue)(\(item["pincode"].stringValue))")
                    if self.selectedPinArrayId.contains(item["id"].stringValue){
                        self.selectedPinNames.append("\(item["name"].stringValue)(\(item["pincode"].stringValue))")
                    }
                }
                self.pinCodeCollection.reloadData()
                self.SetPinDropDown()
            } else {
                
            }
            
        }
        
    }
    
    
    
    func setupUI() {
        topView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = 40
        topView.clipsToBounds = true
        
        pinCodeCollection.delegate = self
        pinCodeCollection.dataSource = self
        ProductListTbl.delegate = self
        ProductListTbl.dataSource    = self
        SetDistrictDropDown()
        SetTalukaDropDown()
        SetPinDropDown()
        getDataFromAPI()
    }
    
    
    func SetDistrictDropDown() {
        
        DistrictOneDrop.anchorView = DistrictOneDropBtn  // UIView or UIBarButtonItem
        DistrictOneDrop.dataSource = DistrictString
        DistrictOneDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            DistrictLbl.text = item
            CityLbl.text = "select taluko"
            PincodeLbl.text = "select pin code"
            selectedDistricut = DistrictJSon[index]["id"].stringValue
            getTalukaData()
            DistrictOneDrop.hide()
            
        }
        
    }
    
    func SetTalukaDropDown() {
        TakuloOneDrop.anchorView = TakuloOneDropBtn  // UIView or UIBarButtonItem
        TakuloOneDrop.dataSource = TakulaString
        TakuloOneDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            CityLbl.text = item
            PincodeLbl.text = "select pin code"
            selectedTaluka = takulaJSon[index]["id"].stringValue
            getPinCodeData()
            TakuloOneDrop.hide()
        }
        
    }
    
    func SetPinDropDown() {
        PinCodeOneDrop.anchorView = PinCodeOneDropBtn// UIView or UIBarButtonItem
        PinCodeOneDrop.dataSource = PinCodeString
        
        
//        PinCodeOneDrop.selectedItem = ""
        PinCodeOneDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            
            if item == "Select all" {
                self.selectedPinNames.removeAll()
                self.selectedPinArrayId.removeAll()
                for i in 0 ..< PinCodeJSon.count {
                    
                    selectedPinArrayId.append(PinCodeJSon[i]["id"].stringValue)
                    self.selectedPinNames.append("\(PinCodeJSon[i]["name"].stringValue)(\(PinCodeJSon[i]["pincode"].stringValue))")
                    self.pinCodeCollection.reloadData()
                    PinCodeOneDrop.hide()
                }
            } else {
                if !selectedPinArrayId.contains(PinCodeJSon[index]["id"].stringValue) {
                    selectedPinArrayId.append(PinCodeJSon[index]["id"].stringValue)
                    self.selectedPinNames.append("\(PinCodeJSon[index]["name"].stringValue)(\(PinCodeJSon[index]["pincode"].stringValue))")
                    self.pinCodeCollection.reloadData()
                    PinCodeOneDrop.hide()
                } else {
                    Toast.show(message: "Already selected", controller: self) { status in
                        
                    }
                }
            }
//            PinCodeOneDrop.selectedItems = selectedPinNames

            
            
        }
    }
    
    
    
}


class productImageCell:UICollectionViewCell {
    
    @IBOutlet weak var crossBtnTap: UIButton!
    @IBOutlet weak var productImg: UIImageView!
    
}

class addProductList:UITableViewCell, UIImagePickerControllerDelegate , UICollectionViewDataSource, UINavigationControllerDelegate, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/3.0
        
        return CGSize(width: 95, height: 95)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SelectedImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productImageCell", for: indexPath) as! productImageCell
        print("in  collectionview cell index path row")
        cell.productImg.image = SelectedImage[indexPath.row]
        cell.crossBtnTap.setTitle("", for: UIControl.State.normal)
        cell.crossBtnTap.setImage(UIImage(named: "cancel"), for: .normal)
        cell.crossBtnTap.tag = indexPath.row
        return cell
    }
    @IBOutlet weak var selectedImgCollection: UICollectionView!
    
    @IBAction func crossBtnTap(_ sender: UIButton) {
        SelectedImage.remove(at: sender.tag)
    }
    @IBOutlet weak var selectectImgHeight: NSLayoutConstraint!
    var SelectedImage:[UIImage] = [] {
        didSet {
            awakeFromNib()
            selectedImgCollection.reloadData()
        }
    }

    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var MainCategoryLbl: UILabel!
    @IBOutlet weak var mainCategoryBtn: UIButton!
    var selectedMainCategoryId = ""
    var mainCategoryString:[String] = []
    var mainCategoryJson:[JSON] = []
    
    @IBOutlet weak var subCategoryLbl: UILabel!
    @IBOutlet weak var subCategoryBtn: UIButton!
    var selectedSubCategoryId = ""
    var subCategoryString:[String] = []
    var subCategoryJson:[JSON] = []
    
    @IBOutlet weak var productLbl: UILabel!
    @IBOutlet weak var productBtn: UIButton!
    var productNameString:[String] = []
    var selectedProductId = ""
    var productJson:[JSON] = []
    @IBOutlet weak var priceTxt: TextField!
    @IBOutlet weak var stockLbl: UILabel!
    @IBOutlet weak var stockBtn: UIButton!
    var StockString:[String] = ["Out of Stock","In Stock"]
    let stockDrop = DropDown()
    var selectedStockValue = ""
    
    @IBOutlet weak var specialPriceTxt: TextField!
    
    @IBOutlet weak var photoPickerBtn: UIButton!
    
    @IBOutlet weak var productImg: UIImageView!
    
    @IBOutlet weak var subView: UIView!
    
 //   @IBOutlet weak var productImageHeight: NSLayoutConstraint!//140
    
    override func awakeFromNib() {
        selectedImgCollection.delegate = self
        selectedImgCollection.dataSource = self
        
        selectectImgHeight.constant = 0

        if SelectedImage.count > 0 {
            selectectImgHeight.constant = 110
            selectedImgCollection.reloadData()
        }
        
        stockLbl.text = StockString[0]
        getupInStockDropDown()
       

        
    }
    
    
    
    @IBAction func stockBtnTap(_ sender: UIButton) {
        stockDrop.show()
    }
    func getupInStockDropDown() {
        
        stockDrop.anchorView = stockBtn// UIView or UIBarButtonItem
        stockDrop.dataSource = StockString
        
        stockDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            selectedStockValue = "\(index)"
            stockLbl.text = "\(StockString[index])"
            
        }
        
        
    }
    
    
}
