//
//  SupplierChangePasswordVC.swift
//  KhanjiMark
//
//  Created by Apple on 05/06/21.
//

import UIKit
import Alamofire

class SupplierChangePasswordVC: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var confirmPasswordTxt: TextField!
    @IBOutlet weak var NewPasswordTxt: TextField!
    @IBOutlet weak var currentPasswordTxt: TextField!
    @IBOutlet weak var updateBtn: UIButton!
    
    var tapGestures = UITapGestureRecognizer()
    var SubtapGestures = UITapGestureRecognizer()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tapGestures.delegate = self
        
        tapGestures = UITapGestureRecognizer(target: self, action: #selector(viewdismiss))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGestures)
        
        
        SubtapGestures = UITapGestureRecognizer(target: self, action: #selector(viewdismiss))
        self.subView.addGestureRecognizer(SubtapGestures)
        self.subView.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
    }
    
    @objc func viewdismiss(recognizer:UIGestureRecognizer) {
        if recognizer.state == .ended {
            if recognizer != SubtapGestures {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func currentShowPassword(_ sender: UIButton) {
        if currentPasswordTxt.isSecureTextEntry {
            sender.setTitle("Hide Password", for: .normal)
            currentPasswordTxt.isSecureTextEntry = false
        } else {
            sender.setTitle("Show Password", for: .normal)
            currentPasswordTxt.isSecureTextEntry = true
        }
    }
    
    @IBAction func showNewPassword(_ sender: UIButton) {
        
        if NewPasswordTxt.isSecureTextEntry {
            sender.setTitle("Hide Password", for: .normal)
            NewPasswordTxt.isSecureTextEntry = false
        } else {
            sender.setTitle("Show Password", for: .normal)
            NewPasswordTxt.isSecureTextEntry = true
        }
    }
    
    @IBAction func ShowConfirmePassword(_ sender: UIButton) {
        if confirmPasswordTxt.isSecureTextEntry {
            sender.setTitle("Hide Password", for: .normal)
            confirmPasswordTxt.isSecureTextEntry = false
        } else {
            sender.setTitle("Show Password", for: .normal)
            confirmPasswordTxt.isSecureTextEntry = true
        }
    }
    
    
    @IBAction func updateBtn(_ sender: UIButton) {
        if validation() {
            let parametr:Parameters = [
                "user_type":userProfile[userProfileKeys().userType].stringValue,
                "user_id":userProfile[userProfileKeys().id].stringValue,
                "current_password":currentPasswordTxt.text ?? "",
                "new_password":NewPasswordTxt.text ?? "",
            ]
            let url = URLS.Comman().ChangePassword
            NetworkManager().getDataFromServer(url: url, method: .post, parameter: parametr, view: self.view){ json in
                print("json \(json)")
                if json["status"].boolValue == true {
                    Toast.show(message: "Password change successfull", controller: self) { (status) in
                        let parameter:Parameters = [
                            "username":userProfile[userProfileKeys().mobile_number].stringValue,
                            "password":self.NewPasswordTxt.text ?? "",
                            "user_type":userProfile[userProfileKeys().userType].stringValue,
                        ]
                       
                        NetworkManager().getDataFromServer(url: URLS.Comman().login, method: .post, parameter: parameter, view: self.view) { json in
                            if json["status"].boolValue == true {
                                   let userJson = json["data"].self
                                   addUserJSONDataToUserDefaults(userData: userJson)
                                   userProfile = getCachedUserJSONData()!
                            }
                            
                        }
                        self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                        
                    }
                }
                
            }
            
        }
    }
    
    func validation() -> Bool {
        print("user password \(userProfile[userProfileKeys().password].stringValue)")
        print("currentPasswordTxt \(currentPasswordTxt.text ?? "")")
        if currentPasswordTxt.text == "" {
            Toast.show(message: "Please enter current password", controller: self) { status in
                self.currentPasswordTxt.becomeFirstResponder()
                
            }
            return false
        }
       else if NewPasswordTxt.text == "" {
            Toast.show(message: "Please enter new password", controller: self) { status in
                self.NewPasswordTxt.becomeFirstResponder()
                
            }
            return false
        }
       else if confirmPasswordTxt.text == "" {
            Toast.show(message: "Please enter confirm Password", controller: self) { status in
                self.confirmPasswordTxt.becomeFirstResponder()
                
            }
            return false
       } else if currentPasswordTxt.text?.lowercased() != userProfile[userProfileKeys().password].stringValue.lowercased() {
            Toast.show(message: "enterd current Password is wrong", controller: self) { status in
                self.currentPasswordTxt.becomeFirstResponder()
            }
            return false
        }
        
        else if NewPasswordTxt.text != confirmPasswordTxt.text {
            Toast.show(message: "confirm Password does not match", controller: self) { status in
                self.NewPasswordTxt.becomeFirstResponder()
                
            }
            return false
        } else {
            return true
        }
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
