//
//  SupplierOrderReceiverdVC.swift
//  KhanjiMark
//
//  Created by Apple on 05/06/21.
//

import UIKit
import SwiftyJSON
import PMAlertController
class SupplierOrderReceiverdVC: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var orderListTbl: UITableView!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var topView: UIView!
    var wherToCome = ""
    var reveiceOrderList = [JSON]() {
        didSet {
            if self.reveiceOrderList.count > 0 {
                self.noDataView.isHidden = true
            } else  {
                self.noDataView.isHidden = false
            }
            self.orderListTbl.reloadData()
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        
        if userProfile[userProfileKeys().userType].stringValue == UserType().Supplier {
            titleLbl.text = "RECEIVED ORDER"
        } else {
            titleLbl.text = "MY ORDER"
        }
        topView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = 40
        topView.clipsToBounds = true
        
        noDataView.isHidden = false
        
        orderListTbl.delegate = self
        orderListTbl.dataSource = self
        
        getData()
    }
    
    func getData() {
        var url  = ""
        print("userProfile id \(userProfile[userProfileKeys().id].stringValue)")
        let parameter = [
            "user_id":userProfile[userProfileKeys().id].stringValue,
        ]
        if userProfile[userProfileKeys().userType].stringValue == UserType().Supplier {
            url = URLS.SellerSide().getMySupplierOrder
        } else  {
            url = URLS.Consumer().orderList
        }
        
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
                
            if json["status"].boolValue == true {
                self.reveiceOrderList = json["data"].arrayValue
                if self.reveiceOrderList.count > 0 {
                    self.noDataView.isHidden = true
                }
                self.orderListTbl.reloadData()
                
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
        }
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func orderStatusBtnTap(_ sender: UIButton) {
        if userProfile[userProfileKeys().userType].stringValue == UserType().Customer {
            let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ConsumerOrderReceivedCancelVC") as! ConsumerOrderReceivedCancelVC
            detailsVC.detailsjson = reveiceOrderList[sender.tag]
            detailsVC.modalPresentationStyle = .overFullScreen
            detailsVC.modalTransitionStyle = .crossDissolve
            self.present(detailsVC, animated: true, completion: nil)
        } else {
            let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "SupplierReceiveOrderStatusVC") as! SupplierReceiveOrderStatusVC
            detailsVC.detailsjson = reveiceOrderList[sender.tag]
            detailsVC.modalPresentationStyle = .overFullScreen
            detailsVC.modalTransitionStyle = .crossDissolve
            self.present(detailsVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func inVoiceBtnTap(_ sender: UIButton) {
        
        guard let url = URL(string: self.reveiceOrderList[sender.tag]["invoice"].stringValue.replacingOccurrences(of: " ", with: "%20")) else { return  }
//        guard let url = URL(string: "your web pdf file link here or you can set from local directory path")
//        else do { return }
        UIApplication.shared.open(url)
//        showActivityWithMessage(message: "", inView: self.view)
//        downloadAudioFile(url: url!.absoluteString,fileName: self.reveiceOrderList[sender.tag]["order_id"].stringValue,main: "Default") { url in
//            print("url\(url)")
//            DispatchQueue.main.async {
//                hideActivity()
//
//            }
//            Toast.show(message: "Download successfull", controller: self) { status in
//
//            }
//
//
//        }
        
    }
    
    @IBAction func markDefaultBtnTap(_ sender: UIButton) {
        let alertVC = PMAlertController(title: "Confirm order delivery", description: "you are about to confir order delivery, Do you wish to continue?", image: nil, style: .alert)

        alertVC.addAction(PMAlertAction(title: "No", style: .cancel, action: { () -> Void in
                    print("Capture action Cancel")
                }))

        alertVC.addAction(PMAlertAction(title: "YES", style: .default, action: { () in
            print("Capture action ok")
            
                var url  = ""
                let parameter = [
                    "user_id":userProfile[userProfileKeys().id].stringValue,
                ]
                url = URLS.Consumer().orderStatusUpdate
                NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
                        
                    if json["status"].boolValue == true {
                        self.getData()
                    } else {
                        Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                        }
                    }
                }
                }))

        self.present(alertVC, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SupplierOrderReceiverdVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reveiceOrderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "suppelierReceivedCell", for: indexPath) as! suppelierReceivedCell
        let item = reveiceOrderList[indexPath.row]
        
        cell.productImg.imageFromUrl(message: "", urlString: item["image"].stringValue) { (status) in
            
        }
        cell.OrderIdLbl.text = "Order id:\(item["order_id"].stringValue)"
        cell.amountLbl.text = "Amount:₹\(item["paid_amt"].stringValue)"
        cell.orderNameLbl.text = "\(item["product_name"].stringValue)"
        cell.dateLbl.text = "date: \("\(item["created"].stringValue)")"
        var status = ""
        
        
        if userProfile[userProfileKeys().userType].stringValue == UserType().Supplier {
            cell.nameLbl.text = "customer: \("\(item["company_name"].stringValue)")"
            cell.emailLbl.text = "Email: \(item["email"].stringValue)"
            cell.numberLbl.text = "Phone: \(item["mobile_number"].stringValue)"
            status = item["status"].stringValue
            cell.orderStatusLbl.text = "Update order status"
//            cell.OrderstatusBtn.setTitle("Update order status", for: .normal)
            cell.markHeight.constant = 0
            cell.markTop.constant = 0
        } else  {
            cell.nameLbl.text = "Supplier: \("\(item["supplier_code"].stringValue)")"
            cell.orderStatusLbl.text = "Cancel order"
//            cell.OrderstatusBtn.setTitle("Cancel order", for: .normal)
            status = item["delivery_status"].stringValue
            cell.emailLbl.text = ""
            cell.numberLbl.text = ""
           
        }
        
        if status == "cancel" {
            cell.productStatus.text = "Order Cancelled"
            cell.productStatus.textColor = #colorLiteral(red: 0.8517047763, green: 0.3168769479, blue: 0.3209982514, alpha: 1)
            cell.orderBtnSwidth.constant = -200
            cell.orderStatusLbl.isHidden = true
            cell.OrderstatusBtn.isHidden = true
            cell.markHeight.constant = 0
            cell.markTop.constant = 0
            cell.markDefaulBtn.isHidden = true
        } else if status == "completed" || status == "delivered"  {
            if status == "completed" {
                cell.productStatus.textColor = #colorLiteral(red: 0.300834775, green: 0.5610468984, blue: 0.2899864316, alpha: 1)
                cell.productStatus.text = "Order Completed"
                cell.markHeight.constant = 0
                cell.markTop.constant = 0
                
                cell.markDefaulBtn.isHidden = true
            } else  {
                cell.productStatus.textColor = #colorLiteral(red: 0, green: 0.1490196078, blue: 0.3647058824, alpha: 1)
                cell.productStatus.text = "Order Delivered"
                cell.markHeight.constant = 20
                cell.markTop.constant = 8
                cell.markDefaulBtn.isHidden = false
            }
            cell.orderBtnSwidth.constant = -200
            cell.orderStatusLbl.isHidden = true
            cell.OrderstatusBtn.isHidden = true
        } else {
            cell.orderStatusLbl.isHidden = false
            cell.OrderstatusBtn.isHidden = false
            cell.orderBtnSwidth.constant = 0
            cell.productStatus.text = "Order Pendding"
            cell.productStatus.textColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
            cell.markHeight.constant = 0
            cell.markTop.constant = 0
            
            cell.markDefaulBtn.isHidden = true
        }
        
        cell.markDefaulBtn.tag = indexPath.row
        cell.InVoiceBtn.tag = indexPath.row
        cell.OrderstatusBtn.tag = indexPath.row
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "SupplierReceiveOrderStatusVC") as! SupplierReceiveOrderStatusVC
        detailsVC.detailsjson = reveiceOrderList[indexPath.row]
        detailsVC.modalPresentationStyle = .overFullScreen
        detailsVC.modalTransitionStyle = .crossDissolve
        self.present(detailsVC, animated: true, completion: nil)
    }
    
}


class suppelierReceivedCell:UITableViewCell {

    @IBOutlet weak var markHeight: NSLayoutConstraint! // 20
    @IBOutlet weak var markTop: NSLayoutConstraint! // 8
    @IBOutlet weak var markDefaulBtn: UIButton!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productStatus: UILabel!
    
    @IBOutlet weak var OrderIdLbl: UILabel!
    @IBOutlet weak var orderNameLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var customerNameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var InVoiceBtn: UIButton!
    
    @IBOutlet weak var OrderstatusBtn: UIButton!
    
    @IBOutlet weak var orderStatusLbl: UILabel!
    
    @IBOutlet weak var orderBtnSwidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        
    }
}
