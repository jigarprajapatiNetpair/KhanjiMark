//
//  SupplierProductDetailsVC.swift
//  KhanjiMark
//
//  Created by Apple on 06/06/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import DropDown
class SupplierProductDetailsVC: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var pinCodeCollection: UICollectionView!
    @IBOutlet weak var DistrictLbl: UILabel!
    @IBOutlet weak var CityLbl: UILabel!
    
    @IBOutlet weak var PincodeLbl: UILabel!
    @IBOutlet weak var DistrictOneDropBtn: UIButton!
    @IBOutlet weak var TakuloOneDropBtn: UIButton!
    @IBOutlet weak var PinCodeOneDropBtn: UIButton!
    
    @IBOutlet weak var MainCategoryLbl: UILabel!
    @IBOutlet weak var mainCategoryBtn: UIButton!
    
    var mainCategoryJson:[JSON] = []
    var mainCategoryString:[String] = []
    let mainCategoryeDrop = DropDown()
    var selectedMainCategory = ""
    
    @IBOutlet weak var subCategoryLbl: UILabel!
    @IBOutlet weak var subCategoryBtn: UIButton!
    
    var subCategoryString:[String] = []
    var subCategoryJson:[JSON] = []
    let subCategoryDrop = DropDown()
    var selectedSubCategory = ""
    
    @IBOutlet weak var productLbl: UILabel!
    @IBOutlet weak var productBtn: UIButton!
    
    var productNameString:[String] = []
    var productJson:[JSON] = []
    let productDrop = DropDown()
    var selectedProductname = ""
    
    @IBOutlet weak var priceTxt: TextField!
    @IBOutlet weak var stockLbl: UILabel!
    @IBOutlet weak var stockBtn: UIButton!
    var StockString:[String] = ["Out of Stock","In Stock"]
    let stockDrop = DropDown()
    var selectedStockValue = ""
    
    @IBOutlet weak var specialPriceTxt: TextField!
    
    @IBOutlet weak var photoPickerBtn: UIButton!
    
    @IBOutlet weak var ProductimageHeight: NSLayoutConstraint! // 160
    @IBOutlet weak var productImg: UIImageView!
    
    @IBOutlet weak var updatebtn: UIButton!
    
    var DistrictString:[String] = []
    let DistrictOneDrop = DropDown()
    var selectedDistricut = ""
    
    var TakulaString:[String] = []
    let TakuloOneDrop = DropDown()
    var selectedTaluka = ""
    
    var PinCodeString:[String] = []
    let PinCodeOneDrop = DropDown()
    var selectedPinNames:[String] = []
    var selectedPinArrayId:[String] = []
    
    
    
    var DistrictJSon = [JSON]()
    var takulaJSon = [JSON]()
    var PinCodeJSon = [JSON]()
    
    
    var productDetails = JSON()
    var selectedProductID = ""
    var selectedMainProductID = ""
    var imagePicker = UIImagePickerController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        DistrictLbl.text = "Select District"
        CityLbl.text = "Select Taluko"
        PincodeLbl.text = "Select Pincode"
        MainCategoryLbl.text = ""
        subCategoryLbl.text = ""
        productLbl.text = ""
        priceTxt.text = ""
        
        
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func SiteDictrictDropDownBtnTap(_ sender: UIButton) {
        DistrictOneDrop.show()
    }
    
    @IBAction func SiteCityDropDownBtnTap(_ sender: UIButton) {
        TakuloOneDrop.show()
    }
    
    @IBAction func SitePinCodeDropDownBtnTap(_ sender: UIButton) {
        PinCodeOneDrop.show()
    }
    
    
    @IBAction func pinCodeDeleteBtnTap(_ sender: UIButton) {
        selectedPinNames.remove(at: sender.tag)
        selectedPinArrayId.remove(at: sender.tag)
        pinCodeCollection.reloadData()
    }
    
    @IBAction func MainCategoryDropBtn(_ sender: UIButton) {
        mainCategoryeDrop.show()
    }
    
    
    @IBAction func subCategoryDownBtn(_ sender: UIButton) {
        subCategoryDrop.show()
    }
    
    
    
    @IBAction func productDropBtn(_ sender: UIButton) {
        productDrop.show()
    }
    
    @IBAction func stockDropBtnTap(_ sender: UIButton) {
        stockDrop.show()
    }
    
    
    
    @IBAction func updateBtnTap(_ sender: UIButton) {
        
        
//        print("selectedMainCategory \(selectedMainCategory)")
//        print("selectedSubCategory \(selectedSubCategory)")
//        print("selectedProductName \(selectedProductname)")
//        print("selectedStockValue \(selectedStockValue)")
//        print("selectedDistricut \(selectedDistricut)")
//        print("selectedTaluka \(selectedTaluka)")
//        print("selectedPinNames \(selectedPinNames.joined(separator: ","))")
//        print("selectedPinArrayId \(selectedPinArrayId.joined(separator: ","))")
//        print("product Spacial Price \(specialPriceTxt.text ?? "")")
//        print("product min Price \(priceTxt.text ?? "")")
//
//        print("product_price_per_km \(productDetails["product_price_per_km"].stringValue)")
//        print("product_price_per_ton \(productDetails["product_price_per_ton"].stringValue)")
//        print("product_price_type \(productDetails["product_price_type"].stringValue)")
        
        
        let paramter:Parameters = [
            "product_category":selectedMainCategory,
            "product_sub_category":selectedSubCategory,
            "product_id":selectedProductID,
            "product_price_per_ton":"1",
            "product_price_per_km":"1",
            "product_district":selectedDistricut,
            "product_taluka":selectedTaluka,
            "product_special_price":specialPriceTxt.text ?? "",
            "product_pincode":selectedPinArrayId.joined(separator: ","),
            "in_stock":selectedStockValue,
            "min_order_qty":priceTxt.text ?? "",
            "user_id":userProfile[userProfileKeys().id].stringValue,
            "supplier_product_id":selectedMainProductID,
        ]
        print("paramter \(paramter)")
        
        let url = URL(string: URLS.SellerSide().productUpdate)
        print("paramter \(paramter)")
        print("url \(url)")
        showActivityWithMessage(message: "", inView: self.view)
        AF.upload(multipartFormData: { multipartFormData in
            
            for (ker, value) in paramter{
                multipartFormData.append((value as! String).data(using: .utf8)!, withName: ker)
                   }
            
            multipartFormData.append((self.productImg.image?.jpegData(compressionQuality: 1))!, withName: "suppliers_product_image", fileName: "\(Date.init().timeIntervalSince1970).png", mimeType: "image/png")
        },
        to: url as! URLConvertible, method: .post , headers: nil)
        .responseString(completionHandler: { (response) in
            hideActivity()
            print("response \(response)")
            let json = JSON(response.data!)
            print("json \(json)")
            if json["status"].boolValue == true {
                Toast.show(message: "Product update successfully", controller: self) { status in
                    self.navigationController?.popViewController(animated: true)
                }
                        } else {
                            Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
            //
                            }
            //            }
                        }
        })
        
        
//        NetworkManager().getDataFromServerWithMultiPart(endUrl: url,method: .post, parameters: paramter, imageData: productImg.image) { json in
//            if json["status"].boolValue == true {
//
//            } else {
//                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
//
//                }
//            }
//        }
    }
    
    
    @IBAction func imagePickerBtnTap(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))

            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))

            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

            /*If you want work actionsheet on ipad
            then you have to use popoverPresentationController to present the actionsheet,
            otherwise app will crash on iPad */
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender
                alert.popoverPresentationController?.sourceRect = sender.bounds
                alert.popoverPresentationController?.permittedArrowDirections = .up
            default:
                break
            }

            self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
       if let pickedImage = info[.originalImage] as? UIImage {
            productImg.image = pickedImage
       }
       picker.dismiss(animated: true, completion: nil)
   }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension SupplierProductDetailsVC:UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedPinNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pincodeTagCell", for: indexPath) as! pincodeTagCell
        cell.nameLbl.text = selectedPinNames[indexPath.row]
        
        return cell
    }
    
    
}


extension SupplierProductDetailsVC {
    func setupUI() {
        topView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = 40
        topView.clipsToBounds = true
        
        
        SetDistrictDropDown()
        SetTalukaDropDown()
        SetPinDropDown()
        getData()
//        getDataFromAPI()
        pinCodeCollection.delegate = self
        pinCodeCollection.dataSource = self
    }
    
    
    func SetDistrictDropDown() {
        
        DistrictOneDrop.anchorView = DistrictOneDropBtn  // UIView or UIBarButtonItem
        DistrictOneDrop.dataSource = DistrictString
        DistrictOneDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            DistrictLbl.text = item
            CityLbl.text = "select taluko"
            PincodeLbl.text = "select pin code"
            selectedDistricut = DistrictJSon[index]["id"].stringValue
            getTalukaData()
            DistrictOneDrop.hide()
            
        }
        
    }
    
    func SetTalukaDropDown() {
        TakuloOneDrop.anchorView = TakuloOneDropBtn  // UIView or UIBarButtonItem
        TakuloOneDrop.dataSource = TakulaString
        TakuloOneDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            CityLbl.text = item
            PincodeLbl.text = "select pin code"
            selectedTaluka = takulaJSon[index]["id"].stringValue
            getPinCodeData()
            TakuloOneDrop.hide()
        }
        
    }
    
    func SetPinDropDown() {
        PinCodeOneDrop.anchorView = PinCodeOneDropBtn// UIView or UIBarButtonItem
        PinCodeOneDrop.dataSource = PinCodeString
        
        PinCodeOneDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            if !selectedPinArrayId.contains(PinCodeJSon[index]["id"].stringValue) {
                selectedPinArrayId.append(PinCodeJSon[index]["id"].stringValue)
                self.selectedPinNames.append("\(PinCodeJSon[index]["name"].stringValue)(\(PinCodeJSon[index]["pincode"].stringValue))")
                self.pinCodeCollection.reloadData()
                PinCodeOneDrop.hide()
            } else {
                Toast.show(message: "Already selected", controller: self) { status in
                    
                }
            }
            
        }
    }
    
    
    func getData() {
        let prarameter = [
            "product_id":selectedMainProductID
        ]
        let url = URLS.SellerSide().productDetails
        
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: prarameter, view: self.view) { json in

            
            if json["status"].boolValue == true {
                self.productDetails = json["data"].self
                print("self.productDetails \(self.productDetails)")
                self.selectedPinArrayId = self.productDetails["pincode"].stringValue.components(separatedBy: ",")
                
                print("\(self.selectedPinArrayId)")
                self.selectedDistricut = self.productDetails["district"].stringValue
                self.selectedTaluka = self.productDetails["taluka"].stringValue
                self.selectedMainCategory = self.productDetails["category_id"].stringValue
                self.selectedSubCategory = self.productDetails["subcategory_id"].stringValue
                self.selectedStockValue = self.productDetails["in_stock"].stringValue
                self.priceTxt.text = self.productDetails["min_order_qty"].stringValue
                self.specialPriceTxt.text = self.productDetails["special_price"].stringValue
                self.selectedProductID = self.productDetails["product_id"].stringValue
                self.productImg.imageFromUrl(message: "", urlString:  self.productDetails["image"].stringValue) { status in
                    
                }
                if self.selectedStockValue == "0" {
                    self.stockLbl.text = self.StockString[0]
                } else {
                    self.stockLbl.text = self.StockString[1]
                }
                self.getDataFromAPI()
                self.getMainCategory()
                self.setInStockOutStoc()
            } else {
//                Toast.show(message: "getData \(json["message"].stringValue)", controller: self) { status in
//
//                }
            }
        }
        
    }
    
    
    func getMainCategory() {
        
        NetworkManager().getDataFromServer(url: URLS.Comman().getProductCategory, method: .get, parameter: [:], view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.mainCategoryJson = json["data"].arrayValue
                self.mainCategoryString.removeAll()
                for item in self.mainCategoryJson {
                    if self.selectedMainCategory == item["id"].stringValue {
                        self.MainCategoryLbl.text = item["category"].stringValue
                        self.getSubCategory()
                        break
                    }
                }
                
                for item in self.mainCategoryJson {
                    self.mainCategoryString.append(item["category"].stringValue)
                    
                }
                
                
                self.setMainCategoryDropDown()
              
            } else {
//                Toast.show(message: "getMainCategory \(json["message"].stringValue)", controller: self) { status in
//
//                }
            }
            
        }
        
    }
    
    func setMainCategoryDropDown() {
        
        mainCategoryeDrop.anchorView = mainCategoryBtn// UIView or UIBarButtonItem
        mainCategoryeDrop.dataSource = mainCategoryString
        
        mainCategoryeDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            selectedMainCategory = self.mainCategoryJson[index]["id"].stringValue
            MainCategoryLbl.text = self.mainCategoryJson[index]["category"].stringValue
            
            self.subCategoryJson.removeAll()
            self.subCategoryString.removeAll()
            self.subCategoryLbl.text = ""
            self.selectedSubCategory = ""
            
            self.productJson.removeAll()
            self.productNameString.removeAll()
            self.productLbl.text = ""
            self.selectedProductID = ""
            self.setSubCategoryDropDown()
            getSubCategory()
            
        }
        
    }
    
    
    func getSubCategory() {
        let parameter = [
            "category_id":selectedMainCategory
        ]
        NetworkManager().getDataFromServer(url: URLS.Comman().getProductSubCategory, method: .post, parameter: parameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.subCategoryJson = json["data"].arrayValue
                self.subCategoryString.removeAll()
                for item in self.subCategoryJson {
                    if self.selectedSubCategory == item["id"].stringValue {
                        self.subCategoryLbl.text = item["subcategory"].stringValue
                        
                        self.productJson.removeAll()
                        self.productNameString.removeAll()
                        self.productLbl.text = ""
                        self.setProductListDropDown()
                        if self.selectedSubCategory != "" {
                            self.getProductList()
                        }
                        
                        break
                    }
                }
                
                for item in self.subCategoryJson {
                    self.subCategoryString.append(item["subcategory"].stringValue)
                    
                }
                
                self.setSubCategoryDropDown()
                
                
            } else {

//                Toast.show(message: "getSubCategory \(json["message"].stringValue)", controller: self) { status in
//
//                }
            }
            
        }
        
    }
    
    func setSubCategoryDropDown() {
        subCategoryDrop.anchorView = subCategoryBtn// UIView or UIBarButtonItem
        subCategoryDrop.dataSource = subCategoryString
        
        subCategoryDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            selectedSubCategory = self.subCategoryJson[index]["id"].stringValue
            subCategoryLbl.text = self.subCategoryJson[index]["subcategory"].stringValue
            
            self.productJson.removeAll()
            self.productNameString.removeAll()
            self.productLbl.text = ""
            self.setProductListDropDown()
            getProductList()
        }
        
    }
    
    
    
    
    func getProductList() {
        let parameter = [
            "category_id":selectedMainCategory,
            "sub_category_id":selectedSubCategory
        ]
        NetworkManager().getDataFromServer(url: URLS.Consumer().productList, method: .post, parameter: parameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.productJson = json["data"].arrayValue
                self.productNameString.removeAll()
                for item in self.productJson {
                    if self.selectedProductID == item["id"].stringValue {
                        self.productLbl.text = item["product_name"].stringValue
                        break
                    }
                }
                for item in self.productJson {
                    self.productNameString.append(item["product_name"].stringValue)
                    
                }
                self.setProductListDropDown()
                
            } else {

//                Toast.show(message: "getProductList \(json["message"].stringValue)", controller: self) { status in
//
//                }
            }
            
        }
        
    }
    
    func setProductListDropDown() {
        productDrop.anchorView = productBtn// UIView or UIBarButtonItem
        productDrop.dataSource = productNameString
        
        productDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            selectedProductID = self.productJson[index]["id"].stringValue
            productLbl.text = self.productJson[index]["product_name"].stringValue
        }
        
    }
    
    func setInStockOutStoc() {
        
            stockDrop.anchorView = stockBtn// UIView or UIBarButtonItem
        stockDrop.dataSource = StockString
            
        stockDrop.selectionAction = { [unowned self] (index: Int, item: String) in
            selectedStockValue = "\(index)"
            stockLbl.text = StockString[index]
            }
            
        
    }
    
    
    
    func getDataFromAPI() {
        let siteDistrictParameter = [
            "state_id":"5"
        ]
        
        NetworkManager().getDataFromServer(url: URLS.Comman().GetDistrict, method: .post, parameter: siteDistrictParameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.DistrictJSon = json["data"].arrayValue
                
                self.DistrictString.removeAll()
                
                for item in self.DistrictJSon {
                    if self.selectedDistricut == item["id"].stringValue {
                        self.DistrictLbl.text = item["district"].stringValue
                        self.selectedDistricut = item["id"].stringValue
                        self.getTalukaData()
                        break
                    }
                    
                    self.DistrictString.append(item["district"].stringValue)
                    
                }
                self.SetDistrictDropDown()
                
                
                
            } else {
//                Toast.show(message: "getDataFromAPI \(json["message"].stringValue)", controller: self) { status in
//
//                }
            }
            
        }
        
        
    }
    
    func getTalukaData() {
        
        let siteTalukaParameter = [
            "district_id":self.selectedDistricut
        ]
        
        NetworkManager().getDataFromServer(url: URLS.Comman().GetTaluka, method: .post, parameter: siteTalukaParameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                print("json \(json)")
                self.takulaJSon = json["data"].arrayValue
                self.TakulaString.removeAll()
                for item in self.takulaJSon {
                    self.TakulaString.append(item["taluka"].stringValue)
                    
                    if self.selectedTaluka == item["id"].stringValue {
                        self.CityLbl.text = item["taluka"].stringValue
                        self.selectedTaluka = item["id"].stringValue
                        
                        self.getPinCodeData()
                        break
                    }
                    
                }
                self.SetTalukaDropDown()
            } else {
//                Toast.show(message: "getTalukaData \(json["message"].stringValue)", controller: self) { status in
//
//                }
            }
            
        }
        
    }
    
    func getPinCodeData() {
        let sitePinCodeParameter = [
            "district_id":self.selectedDistricut,
            "taluka_id":self.selectedTaluka
        ]
        
        NetworkManager().getDataFromServer(url: URLS.Comman().GetPincode, method: .post, parameter: sitePinCodeParameter, view: self.view) { json in
            
            if json["status"].boolValue == true {
                self.PinCodeJSon = json["data"].arrayValue
                self.PinCodeString.removeAll()
                for item in self.PinCodeJSon {
                    self.PinCodeString.append("\(item["name"].stringValue)(\(item["pincode"].stringValue)")
                    if self.selectedPinArrayId.contains(item["id"].stringValue){
                        self.selectedPinNames.append("\(item["name"].stringValue)(\(item["pincode"].stringValue))")
                    }
                }
                self.pinCodeCollection.reloadData()
                self.SetPinDropDown()
            } else {
//                Toast.show(message: "getPinCodeData \(json["message"].stringValue)", controller: self) { status in
//
//                }
            }
            
        }
        
    }
    
    
}

class pincodeTagCell:UICollectionViewCell {
    
    @IBOutlet weak var removePin: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
}
