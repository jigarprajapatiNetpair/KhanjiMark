//
//  SupplierReceiveOrderStatusVC.swift
//  KhanjiMark
//
//  Created by Apple on 12/06/21.
//

import UIKit
import DropDown
import SwiftyJSON
import Alamofire
class SupplierReceiveOrderStatusVC: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var orderStatusBtn: UIButton!
    @IBOutlet weak var subView: UIView!
    var OrderStatusStrins = ["pending","loading","shipped","delivered","completed"]
    var detailsjson = JSON()
    var orderdropDown = DropDown()
    var tapGestures = UITapGestureRecognizer()
    var SubtapGestures = UITapGestureRecognizer()
    override func viewDidLoad() {
        super.viewDidLoad()
        setDropDown()
        print("detailsjson \(detailsjson)")
        descriptionLbl.text = "You are going to change delivery staus of order #\(detailsjson["order_id"].stringValue)"
        tapGestures.delegate = self
        orderStatusLbl.text = detailsjson["status"].stringValue
        orderStatusLbl.text = detailsjson["status"].stringValue
        tapGestures = UITapGestureRecognizer(target: self, action: #selector(viewdismiss))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGestures)
        
        SubtapGestures = UITapGestureRecognizer(target: self, action: #selector(viewdismiss))
        self.subView.addGestureRecognizer(SubtapGestures)
        self.subView.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
    }
    
    @objc func viewdismiss(recognizer:UIGestureRecognizer) {
        if recognizer.state == .ended {
            if recognizer != SubtapGestures {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func setDropDown(){
        orderdropDown.anchorView = orderStatusBtn  // UIView or UIBarButtonItem
        orderdropDown.textColor = .white
        orderdropDown.backgroundColor = .lightGray
        orderdropDown.textFont = .systemFont(ofSize: 16, weight: .medium)
        orderdropDown.dataSource = OrderStatusStrins
        orderdropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            orderStatusLbl.text = item
        }
    }
    @IBAction func updateBtnTap(_ sender: UIButton) {
        let parameter:Parameters = [
            "id":detailsjson["id"].stringValue,
            "delivery_status":orderStatusLbl.text ?? ""
        ]
        print("parameter \(parameter)")
        let url = URLS.SellerSide().orderStatusUpdate
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
            print("json \(json)")
            if json["status"].boolValue == true {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
        }
    }
    @IBAction func cancelBtnTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func orderStatusBtnTap(_ sender: UIButton) {
        orderdropDown.show()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
