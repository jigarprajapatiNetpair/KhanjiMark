//
//  SupplierProductListVC.swift
//  KhanjiMark
//
//  Created by Apple on 05/06/21.
//

import UIKit
import Alamofire
import SwiftyJSON
class SupplierProductListVC: UIViewController {

    
    @IBOutlet weak var productListTbl: UITableView!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var topView: UIView!
    
    var myprdoductList = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI() {
        
        topView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = 40
        topView.clipsToBounds = true
        
        
        noDataView.isHidden = false
        productListTbl.delegate = self
        productListTbl.dataSource = self
        
    }

    override func viewWillAppear(_ animated: Bool) {
        getData()
    }
    func getData() {
        let parameter = [
            "supplier_id":userProfile[userProfileKeys().id].stringValue,
        ]
        let url = URLS.SellerSide().getMySupplierProduct
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
            print("json \(json)")
            if json["status"].boolValue == true {
                self.myprdoductList = json["data"].arrayValue
                self.noDataView.isHidden = true
                self.productListTbl.reloadData()
                
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
        }
    }
    
    @IBAction func editBtnTap(_ sender: UIButton) {
        
    //        selectedProductID
        let item = myprdoductList[sender.tag]
            let productDetails = self.storyboard?.instantiateViewController(withIdentifier: "SupplierProductDetailsVC") as! SupplierProductDetailsVC
            productDetails.selectedMainProductID = item["id"].stringValue
            self.navigationController?.pushViewController(productDetails, animated: true)
            
            
        
    }
    @IBAction func deleteBtn(_ sender: UIButton) {
        let item = myprdoductList[sender.tag]
        
            let parameter = [
                "user_id":userProfile[userProfileKeys().id].stringValue,
                "product_id":item["id"].stringValue
            ]
            let url = URLS.SellerSide().productDelete
            NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
                print("json \(json)")
                if json["status"].boolValue == true {
                    self.myprdoductList.remove(at: sender.tag)
                    self.productListTbl.reloadData()
                } else {
                    Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                        
                    }
                }
            }
        
        
    }
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SupplierProductListVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myprdoductList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productListCell", for: indexPath) as! productListCell
        let item = myprdoductList[indexPath.row]
        
        
        cell.productImg.imageFromUrl(message: "", urlString: item["image"].stringValue) { (status) in
            
        }
        cell.productNameLbl.text = item["product_name"].stringValue
        cell.categoryLbl.text = "\(item["assign_pincode"].stringValue)"
//        cell.priceLbl.text = "₹\(item["special_price"].stringValue)"
        
        cell.deleteBtn.tag = indexPath.row
        cell.editBtn.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {}
}
class productListCell:UITableViewCell {
    @IBOutlet weak var productImg: UIImageView!
    
    @IBOutlet weak var productNameLbl: UILabel!
    
    @IBOutlet weak var categoryLbl: UILabel!
   
    @IBOutlet weak var editBtn: UIButton!
    
    @IBOutlet weak var deleteBtn: UIButton!
    override func awakeFromNib() {
        editBtn.layer.maskedCorners = [.layerMinXMaxYCorner]
        editBtn.layer.cornerRadius = 8
        
        deleteBtn.layer.maskedCorners = [.layerMaxXMaxYCorner]
        deleteBtn.layer.cornerRadius = 8
    }
}
