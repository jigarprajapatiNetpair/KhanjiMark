//
//  IntialVC.swift
//  KhanjiMark
//
//  Created by Apple on 31/05/21.
//

import UIKit

class IntialVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.applyGradient(colours: [#colorLiteral(red: 0.9808695912, green: 0.8293440938, blue: 0.02412255481, alpha: 1),#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)], locations: [0.0,1.0])
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cunsumerBtnTap(_ sender: UIButton) {
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        loginVC.selectedUsertype = UserType().Customer
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    @IBAction func suppilerBtnTap(_ sender: UIButton) {
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        loginVC.selectedUsertype = UserType().Supplier
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
