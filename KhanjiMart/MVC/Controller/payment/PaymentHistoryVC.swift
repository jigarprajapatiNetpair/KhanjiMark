//
//  PaymentHistoryVC.swift
//  KhanjiMark
//
//  Created by RMV on 8/4/21.
//

import UIKit
import Alamofire
import SwiftyJSON
import AppInvokeSDK
class PaymentHistoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource,AIDelegate {
    func didFinish(with status: AIPaymentStatus, response: [String : Any]) {
        print("🔶 Paytm Callback Response: ", response)
    }
    
    
    let name = ["Ela Barnett","lsaiah Francis","Lula Briggs"]
    let code = ["#740136","#539642","#123146"]
    let price = ["$25.00","$12.00","$34.00"]
    
    @IBOutlet weak var paymentWidthdrawBtn: UIButton!
    var getTransacationHistory = [JSON]() {
        didSet{
            if getTransacationHistory.count > 0 {
                paymentHistoryTbl.isHidden = false
                paymentHistoryTbl.delegate = self
                paymentHistoryTbl.dataSource = self
                paymentHistoryTbl.reloadData()
            } else {
                paymentHistoryTbl.isHidden = false
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getTransacationHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "paymenthistoryCell", for: indexPath) as! paymenthistoryCell
        
        
        
        cell.userNameLbl.text = getTransacationHistory[indexPath.row]["comment"].stringValue
//        cell.userCodeLbl.text = "#\(getTransacationHistory[indexPath.row]["id"].stringValue)"
//        cell.dateLbl.text = getTransacationHistory[indexPath.row]["created_date"].stringValue
        cell.ModeLbl.text = getTransacationHistory[indexPath.row]["order_payment_mode"].stringValue
        
        cell.dateLbl.text = convertDateTimeFormate(dateformate: "YYYY-MM-dd hh:mm:ss", convertedFormate: "dd MMM YYY hh:mm a", date: getTransacationHistory[indexPath.row]["created_date"].stringValue)
        
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: Double("\(getTransacationHistory[indexPath.row]["amount"].stringValue)") as! NSNumber) {
            cell.userPriceLbl.text = "\(formattedTipAmount)"
        }

        if let formattedTipAmount = formatter.string(from: Double("\(getTransacationHistory[indexPath.row]["commission_fee"].stringValue)") as! NSNumber) {
            cell.commissionFeeLbl.text = formattedTipAmount
        }
        if let formattedTipAmount = formatter.string(from: Double("\(getTransacationHistory[indexPath.row]["tcs_amount"].stringValue)") as! NSNumber) {
            cell.TCSLbl.text = formattedTipAmount
        }
        if let formattedTipAmount = formatter.string(from: Double("\(getTransacationHistory[indexPath.row]["gst_amount"].stringValue)") as! NSNumber) {
            cell.GSTLbl.text = formattedTipAmount
        }
        
        
        return cell
    }
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var totalEarnCountLbl: UILabel!
    @IBOutlet weak var paymentHistoryTbl: UITableView!
    private let appInvoke = AIHandler()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.paymentHistoryTbl.isHidden = true
        self.totalEarnCountLbl.text = ""
        getData()
        topView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        topView.layer.cornerRadius = 40
        topView.clipsToBounds = true
        paymentHistoryTbl.delegate = self
        paymentHistoryTbl.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    var TotalAmount:Double = 0.0
    func getData() {
        let url = URLS.paytm().walletAmount
        let parameter:Parameters = [
            "user_id":userProfile[userProfileKeys().id].stringValue
        ]
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
            if json["status"].boolValue{
                
                self.getTransacationHistory = json["data"]["transaction"].arrayValue
                
                let formatter = NumberFormatter()
                formatter.locale = Locale.current
                formatter.numberStyle = .currency
                if let formattedTipAmount = formatter.string(from: json["data"]["balance"].doubleValue as NSNumber) {
                    self.TotalAmount = json["data"]["balance"].doubleValue
                    self.totalEarnCountLbl.text = "\(formattedTipAmount)"
                }
                if json["data"]["is_requested"].stringValue == "yes" {
                    self.paymentWidthdrawBtn.isUserInteractionEnabled = false
                    self.paymentWidthdrawBtn.backgroundColor = .darkGray
                } else {
                    self.paymentWidthdrawBtn.isUserInteractionEnabled = true
                    self.paymentWidthdrawBtn.backgroundColor = AppColors.AppColor
                }

            } else {
                self.paymentHistoryTbl.isHidden = true
                self.totalEarnCountLbl.text = ""
            }
        }
    }
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func withDrawPaymentBtnTap(_ sender: UIButton) {

        withDrawAmount()
    }
    
    func withDrawAmount() {
        let url = URLS.paytm().withDrawAmount
        let parameter:Parameters = [
            "user_id":userProfile[userProfileKeys().id].stringValue,
            "amount":TotalAmount
        ]
        NetworkManager().getDataFromServer(url: url, method: .post, parameter: parameter, view: self.view) { json in
            if json["status"].boolValue{
                self.getData()
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            } else {
                Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                    
                }
            }
        }
    }
    
    func openPaymentWebVC(_ controller: UIViewController?) {
        
            if let vc = controller {
                DispatchQueue.main.async {[weak self] in
                    self?.present(vc, animated: true, completion: nil)
                }
            }
        }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class paymenthistoryCell:UITableViewCell {
    
    @IBOutlet weak var userPriceLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    
    @IBOutlet weak var ModeLbl: UILabel!
    @IBOutlet weak var TCSLbl: UILabel!
    @IBOutlet weak var GSTLbl: UILabel!
    @IBOutlet weak var commissionFeeLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    override class func awakeFromNib() {
        
    }
}
