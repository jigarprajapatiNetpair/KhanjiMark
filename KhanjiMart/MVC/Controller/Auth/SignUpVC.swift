//
//  SignUpVC.swift
//  KhanjiMark
//
//  Created by Apple on 01/06/21.
//

import UIKit
import DropDown
import Alamofire
class SignUpVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var MrMsLbl: UILabel!
    @IBOutlet weak var dropdownBtn: UIButton!
    
    
    // TextField
    @IBOutlet weak var firstNameTxt: TextField!
    @IBOutlet weak var LastNameTxt: TextField!
    @IBOutlet weak var CompanyNameTxt: TextField!
    @IBOutlet weak var EmailIdTxt: TextField!
    @IBOutlet weak var MobileNumTxt: TextField!
    @IBOutlet weak var PasswordTxt: TextField!
    @IBOutlet weak var ConfirmPasswordTxt: TextField!
    
    @IBOutlet weak var passwordShowBtn: UIButton!
    @IBOutlet weak var confirmPasswordBtn: UIButton!
    
    var selectedUsertype = ""
    let MrMsArray = ["Mr.","Mrs.","Ms."]
    let dropdown = DropDown()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI() {
        MobileNumTxt.delegate = self
        setupDropDown()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == MobileNumTxt && string != "" {
            return !(MobileNumTxt.text?.isPhoneNumber ?? false)
        } else {
             return true
        }
    }
    
    
    func setupDropDown() {
        dropdown.anchorView = dropdownBtn // UIView or UIBarButtonItem
        dropdown.dataSource = MrMsArray
        dropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            MrMsLbl.text = item
            dropdown.hide()
        }
    }
    
    
    @IBAction func dropdownBtnTap(_ sender: UIButton) {
        dropdown.show()
    }
    
    @IBAction func passwordShowBtn(_ sender: UIButton) {
        if PasswordTxt.isSecureTextEntry {
            sender.setTitle("Hide Password", for: .normal)
            PasswordTxt.isSecureTextEntry = false
        } else {
            sender.setTitle("Show Password", for: .normal)
            PasswordTxt.isSecureTextEntry = true
        }
    }
    
    @IBAction func confirmPassword(_ sender: UIButton) {
        if ConfirmPasswordTxt.isSecureTextEntry {
            sender.setTitle("Hide Password", for: .normal)
            ConfirmPasswordTxt.isSecureTextEntry = false
        } else {
            sender.setTitle("Show Password", for: .normal)
            ConfirmPasswordTxt.isSecureTextEntry = true
        }
    }
    
    
    @IBAction func signUpBtn(_ sender: UIButton) {
        if validation() {
            let parameter:Parameters = [
                "title":MrMsLbl.text ?? "",
            "first_name":firstNameTxt.text ?? "",
            "last_name":LastNameTxt.text ?? "",
            "mobile_number":MobileNumTxt.text ?? "",
            "email":EmailIdTxt.text ?? "",
            "company_name":CompanyNameTxt.text ?? "",
            "password":PasswordTxt.text ?? "",
            "user_type":selectedUsertype,
         ]
            
            NetworkManager().getDataFromServer(url: URLS.Comman().registration, method: .post, parameter: parameter, view: self.view) { json in
                print("json \(json)")
                if json["status"].boolValue == true {
                    Toast.show(message: "Registration successfull", controller: self) { (status) in
                       
                        let userJson = json["data"].self
                        addUserJSONDataToUserDefaults(userData: userJson)
                        userProfile = getCachedUserJSONData()!
                        
                        let otpview = self.storyboard?.instantiateViewController(withIdentifier: "OTPView") as! OTPView
                        otpview.mobileNumber = self.MobileNumTxt.text ?? ""
                        otpview.SelecteduseryType = self.selectedUsertype
                        otpview.user_Id = userProfile[userProfileKeys().id].stringValue
                        self.navigationController?.pushViewController(otpview, animated: true)
                    }
                } else {
                    Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                        
                    }
                }
                
            }
            
            
        }
    }
    @IBAction func haveAccountBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func validation() -> Bool {
        if firstNameTxt.text == "" {
            Toast.show(message: "Please enter First name", controller: self) { status in
                
            }
            return false
        } else if LastNameTxt.text == "" {
            Toast.show(message: "Please enter last name", controller: self) { status in
                
            }
            return false
        } else if EmailIdTxt.text == "" {
            Toast.show(message: "Please enter email address", controller: self) { status in
                
            }
            return false
        }  else if MobileNumTxt.text == "" {
            Toast.show(message: "Please enter mobile number", controller: self) { status in
                
            }
            return false
        } else if !(MobileNumTxt.text?.isPhoneNumber ?? false) {
            Toast.show(message: "Please enter mobile valid number", controller: self) { status in
                
            }
            return false
        } else if PasswordTxt.text == "" {
            Toast.show(message: "Please enter password", controller: self) { status in
                
            }
            return false
        }  else if ConfirmPasswordTxt.text == "" {
            Toast.show(message: "Please confirm password", controller: self) { status in
                
            }
            return false
        }   else if PasswordTxt.text != ConfirmPasswordTxt.text {
            Toast.show(message: "confirm password does not match", controller: self) { status in
                
            }
            return false
        } else {
            return true
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension String {
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count && self.count == 10
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}
