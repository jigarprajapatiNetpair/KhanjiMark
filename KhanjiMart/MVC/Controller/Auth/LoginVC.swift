//
//  LoginVC.swift
//  KhanjiMark
//
//  Created by Apple on 01/06/21.
//

import UIKit
import Alamofire
class LoginVC: UIViewController {

    @IBOutlet weak var passwordTxt: TextField!
    @IBOutlet weak var mobileEmailTxt: TextField!
    var selectedUsertype = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI() {
        passwordTxt.isSecureTextEntry = true
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SignIn(_ sender: UIButton) {
        if validation() {
            let parameter:Parameters = [
                "username":mobileEmailTxt.text ?? "",
            "password":passwordTxt.text ?? "",
            "user_type":selectedUsertype,
         ]
            
            NetworkManager().getDataFromServer(url: URLS.Comman().login, method: .post, parameter: parameter, view: self.view) { json in
                print("json \(json)")
                if json["status"].boolValue == true {
                    showActivityWithMessage(message: "", inView: self.view)
                    Toast.show(message: "Login successfull", controller: self) { (status) in
                        UserDefaultsManager().isLogIn = true
                        let userJson = json["data"].self
                        addUserJSONDataToUserDefaults(userData: userJson)
                        userProfile = getCachedUserJSONData()!
                        
                        if userProfile[userProfileKeys().userType].stringValue == UserType().Customer {
                            hideActivity()
                            let supplierTab = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController") as!  SWRevealViewController
                            self.navigationController?.pushViewController(supplierTab, animated: true)
                        } else {
                            hideActivity()
                            let supplierTab = self.storyboard?.instantiateViewController(withIdentifier: "SuppilerTabBarVC") as!  SuppilerTabBarVC
                            self.navigationController?.pushViewController(supplierTab, animated: true)
                        }
                        
                        
                        
                    }
                } else {
                    Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                        
                    }
                }
                
            }
            
           
        }
    }
    
    @IBAction func showPasswordBtn(_ sender: UIButton) {
        if passwordTxt.isSecureTextEntry {
            sender.setTitle("Hide Password", for: .normal)
            passwordTxt.isSecureTextEntry = false
        } else {
            sender.setTitle("Show Password", for: .normal)
            passwordTxt.isSecureTextEntry = true
        }
    }
    
    @IBAction func signUpBtnTap(_ sender: UIButton) {
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        signUp.selectedUsertype = selectedUsertype

        self.navigationController?.pushViewController(signUp, animated: true)
    }
    
    func validation() -> Bool {
        if mobileEmailTxt.text == "" {
            Toast.show(message: "Please mobile number or Email id", controller: self) { status in
                
            }
            return false
        } else if passwordTxt.text == "" {
            Toast.show(message: "Please enter password", controller: self) { status in
                
            }
            return false
        } else {
            return true
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
