//
//  OTPView.swift
//  KhanjiMark
//
//  Created by Apple on 02/06/21.
//

import UIKit
import OTPFieldView
import Alamofire
class OTPView: UIViewController {
    
    var enterdText = ""
    var SelecteduseryType = ""
    var user_Id = ""
    @IBOutlet weak var resentLbl: UILabel!
    @IBOutlet weak var OTPView: OTPFieldView!
    var mobileNumber = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        resentLbl.text = "Please type the verification code  sent to \(mobileNumber)"
        setOTPView()
        
    }
    
    func checkRange(_ range: NSRange, contain index: Int) -> Bool {
        return index > range.location && index < range.location + range.length
    }
    
    @IBAction func verifyBtnTap(_ sender: UIButton) {
        print("OTP \(enterdText.count)")
        if enterdText.count == 6 {
            let parameter:Parameters = [
                    "user_id":user_Id,
                    "otp":enterdText,
                    "user_type": SelecteduseryType
           
         ]
            
            NetworkManager().getDataFromServer(url: URLS.Comman().OtpVerify, method: .post, parameter: parameter, view: self.view) { json in
                if json["status"].boolValue == true {
                    Toast.show(message: "Verifivaction successfull", controller: self) { (status) in
                        UserDefaultsManager().isLogIn = true
                        if userProfile[userProfileKeys().userType].stringValue == UserType().Customer {
                            let supplierTab = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController") as!  SWRevealViewController
                            self.navigationController?.pushViewController(supplierTab, animated: true)
                        } else {
                            let supplierTab = self.storyboard?.instantiateViewController(withIdentifier: "SuppilerTabBarVC") as!  SuppilerTabBarVC
                            self.navigationController?.pushViewController(supplierTab, animated: true)
                        }
                    }
                } else {
                    Toast.show(message: "\(json["message"].stringValue)", controller: self) { status in
                        
                    }
                }
                
            }
            
        } else {
            Toast.show(message: "Please enter valid OTP", controller: self) { status in
                self.OTPView.resignFirstResponder()
            }
        }
    }
    
    func setOTPView() {
        
        self.OTPView.fieldsCount = 6
        self.OTPView.fieldBorderWidth = 2
        self.OTPView.defaultBorderColor = UIColor.black
        self.OTPView.filledBorderColor = AppColors.AppColor
        self.OTPView.cursorColor = UIColor.black
        self.OTPView.otpInputType = .numeric
        self.OTPView.displayType = .underlinedBottom
        self.OTPView.fieldSize = UIScreen.main.bounds.width / 7.4
        self.OTPView.fieldFont = UIFont(name: "sf-ui-display-semibold-58646eddcae92", size: 20) ?? UIFont.boldSystemFont(ofSize: 20)
        self.OTPView.separatorSpace = 8
        self.OTPView.delegate = self
        self.OTPView.initializeUI()
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension OTPView:OTPFieldViewDelegate{
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        print("index \(index)")
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        enterdText  = otpString
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        print("hasEnteredAll \(hasEnteredAll)")
        
        return false
    }
}
