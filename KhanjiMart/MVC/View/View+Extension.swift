//
//  View+Extenstion.swift
//  myvpn
//
//  Created by RMV on 01/03/21.
//  Copyright © 2021 TechnoWaves. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import BBWebImage
extension UIView {
    
    @discardableResult
           func applyGradient(colours: [UIColor]) -> CAGradientLayer {
               return self.applyGradient(colours: colours, locations: nil)
           }

           @discardableResult
           func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
               let gradient: CAGradientLayer = CAGradientLayer()
               gradient.frame = self.bounds
               gradient.colors = colours.map { $0.cgColor }
               gradient.locations = locations
               self.layer.insertSublayer(gradient, at: 0)
               return gradient
           }
    // ->1
    enum Direction: Int {
        case topToBottom = 0
        case bottomToTop
        case leftToRight
        case rightToLeft
    }
    
    func startShimmeringAnimation(animationSpeed: Float = 1.4,
                                  direction: Direction = .leftToRight,
                                  repeatCount: Float = MAXFLOAT,
                                  lightColor: CGColor,
                                  darkColor:CGColor) {
        self.stopShimmeringAnimation()
        // Create color  ->2
        let lightColor = lightColor
        let blackColor = darkColor
        
        // Create a CAGradientLayer  ->3
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [blackColor, lightColor, blackColor]
        gradientLayer.frame = CGRect(x: -self.bounds.size.width, y: -self.bounds.size.height, width: 3 * self.bounds.size.width, height: 3 * self.bounds.size.height)
        
        switch direction {
        case .topToBottom:
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
            
        case .bottomToTop:
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
            
        case .leftToRight:
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
        case .rightToLeft:
            gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.5)
        }
        
        gradientLayer.locations =  [0.35, 0.50, 0.65] //[0.4, 0.6]
        self.layer.mask = gradientLayer
        
        // Add animation over gradient Layer  ->4
        CATransaction.begin()
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [0.0, 0.1, 0.2]
        animation.toValue = [0.8, 0.9, 1.0]
        animation.duration = CFTimeInterval(animationSpeed)
        animation.repeatCount = repeatCount

        gradientLayer.add(animation, forKey: "shimmerAnimation")
        CATransaction.commit()
    }
    
    
    
    func stopShimmeringAnimation() {
        
        self.layer.mask = nil
        self.layer.mask?.removeFromSuperlayer()
        self.layer.removeAnimation(forKey: "shimmerAnimation")
        self.layer.removeAllAnimations()
    }
    
    @IBInspectable var cornerRadius: Double {
        get {
            return self.cornerRadius
        }set {
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    @IBInspectable var borderWidth: Double {
        get {
            return Double(self.layer.borderWidth)
        }
        set {
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        }
        set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.shadowOpacity
        }
        set {
//            self.layer.shadowOffset = CGSize(width: 0.0, height: 20.5)
            self.layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var radius: Float {
        get {
            return self.radius
        }
        set {
            self.layer.shadowRadius = CGFloat(newValue)
        }
    }
    
    @IBInspectable var shadowOffsetWidth: Float {
        get {
            return self.shadowOffsetWidth
        }
        set {
            
            self.layer.shadowOffset.width = CGFloat(newValue)
            
        }
    }
    
    @IBInspectable var shadowOffsetHeight: Float {
        get {
            return self.shadowOffsetHeight
        }
        set {
            
            self.layer.shadowOffset.height = CGFloat(newValue)
            
        }
    }
    
    
}
extension UIImage {
    func rotate(radians: Float) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!

        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
    func increaseContrast(num:Float) -> UIImage {
        let inputImage = CIImage(image: self)!
        let parameters = [
            "inputContrast": NSNumber(value: num)
        ]
        let outputImage = inputImage.applyingFilter("CIColorControls", parameters: parameters)

        let context = CIContext(options: nil)
        let img = context.createCGImage(outputImage, from: outputImage.extent)!
        return UIImage(cgImage: img)
    }
    func increaseBritness(num:Float) -> UIImage {
        let inputImage = CIImage(image: self)!
        let parameters = [
            "inputBrightness": NSNumber(value: num)
        ]
        let outputImage = inputImage.applyingFilter("CIColorControls", parameters: parameters)

        let context = CIContext(options: nil)
        let img = context.createCGImage(outputImage, from: outputImage.extent)!
        return UIImage(cgImage: img)
    }
}
extension UIImageView {
    public func imageFromUrl(message:String,urlString: String,successBlock:@escaping (_ response: Bool )->Void) {
        self.image = #imageLiteral(resourceName: "user-profile")
        let indicator = UIActivityIndicatorView()
        indicator.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        indicator.alpha  = 0.5
        if #available(iOS 13.0, *) {
            indicator.style = .white
        } else {
            indicator.style = .white
            // Fallback on earlier versions
        }
        indicator.translatesAutoresizingMaskIntoConstraints = false

        self.addSubview(indicator)
        indicator.tintColor = AppColors.AppColor
        indicator.hidesWhenStopped = true
        self.addSubview(indicator)
        
        indicator.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height).isActive = true
        indicator.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
        indicator.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        
        indicator.startAnimating()
        print("urlString \(urlString)")
        if urlString != "" {
            
            let url = URL(string: urlString.replacingOccurrences(of: " ", with: "%20"))!
            self.bb_setImage(with: url,options: .progressiveDownload) { data, count, image in
                
            } completion: { image, Data, Error, caches in
                indicator.stopAnimating()
    //            self.image = image
                successBlock(true)
            }
        } else {
            indicator.stopAnimating()
        }
        

        
    }
}
