//
//  Label+Extenstion.swift
//  myvpn
//
//  Created by RMV on 01/03/21.
//  Copyright © 2021 TechnoWaves. All rights reserved.
//

import Foundation
import UIKit
extension UILabel {
    func textAttribute(firstColor:UIColor,secondColoer:UIColor,firstString:String,secondText:String){
        let attrs1 = [NSAttributedString.Key.foregroundColor : firstColor]

        let attrs2 = [NSAttributedString.Key.foregroundColor : secondColoer]

        let attributedString1 = NSMutableAttributedString(string:firstString, attributes:attrs1)

        let attributedString2 = NSMutableAttributedString(string:secondText, attributes:attrs2)
        attributedString1.append(attributedString2)

        self.attributedText = attributedString1
    }
//    func setColoredLabel() {
//        let string = NSMutableAttributedString(string: self.text!)
//        string.setColorForText(textForAttribute: "red", withColor: #colorLiteral(red: Float(0.9254902005), green: Float(0.2352941185), blue: 0.1019607857, alpha: Float(1)))
//        string.setColorForText(textForAttribute: "blue", withColor: #colorLiteral(red: Float(0.2392156869), green: Float(0.6745098233), blue: 0.9686274529, alpha: Float(1)))
//        string.setColorForText(textForAttribute: "green", withColor: #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: Float(1)))
//        self.attributedText = string
//    }
    
}
extension NSMutableAttributedString {

    func setColorForText(textForAttribute: String, withColor color: UIColor,font:UIFont) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)

        // Swift 4.2 and above
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        self.addAttribute(NSAttributedString.Key.font, value: font, range: range)

        // Swift 4.1 and below
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }

}
extension UILabel {
    func indexOfAttributedTextCharacterAtPoint(point: CGPoint) -> Int {
        assert(self.attributedText != nil, "This method is developed for attributed string")
        let textStorage = NSTextStorage(attributedString: self.attributedText!)
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        let textContainer = NSTextContainer(size: self.frame.size)
        textContainer.lineFragmentPadding = 0
        textContainer.maximumNumberOfLines = self.numberOfLines
        textContainer.lineBreakMode = self.lineBreakMode
        layoutManager.addTextContainer(textContainer)

        let index = layoutManager.characterIndex(for: point, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return index
    }
}
extension String {
    static func format(strings: [String],
                    boldFont: UIFont = UIFont.boldSystemFont(ofSize: 14),
                    boldColor: UIColor = UIColor.blue,
                    inString string: String,
                    font: UIFont = UIFont.systemFont(ofSize: 14),
                    color: UIColor = UIColor.black) -> NSAttributedString {
        let attributedString =
            NSMutableAttributedString(string: string,
                                    attributes: [
                                        NSAttributedString.Key.font: font,
                                        NSAttributedString.Key.foregroundColor: color])
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont, NSAttributedString.Key.foregroundColor: boldColor]
        for bold in strings {
            attributedString.addAttributes(boldFontAttribute, range: (string as NSString).range(of: bold))
        }
        return attributedString
    }
}
