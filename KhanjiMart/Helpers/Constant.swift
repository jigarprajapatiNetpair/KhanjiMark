//
//  Constant.swift
//  KhanjiMark
//
//  Created by Apple on 31/05/21.
//

import Foundation
import UIKit
import Contacts
import AVFoundation
import SwiftyJSON
import Alamofire

var appColor = "00285B"
struct AppColors {
    static var AppColor:UIColor = #colorLiteral(red: 0, green: 0.1568627451, blue: 0.3568627451, alpha: 1)
}

let appName = "Khanji Mart"

var userProfile = JSON()



// paytm access

var merchantId = "yadQop78153254384792"
var channerId = "WAP"
var marchantkey = "2uFgvFWE4PJhJpl_"
var callBackURL = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="
var marchantcode = "12345"

// ACTIVIY INDICATOR

var blackView: UIView? = nil

func showActivityWithMessage(message: String, inView view: UIView) {
    if blackView == nil {
        blackView = UIView(frame: UIScreen.main.bounds)
        blackView!.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        var factor: Float = 1.0
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            factor = 2.0
        }
        
        var rect = CGRect.init()
        rect.size.width = CGFloat(factor * Float(200))
        rect.size.height = CGFloat(factor * Float(70))
        rect.origin.y = CGFloat((blackView?.frame.size.height)! - rect.size.height) / CGFloat(2.0)
        rect.origin.x = CGFloat((blackView?.frame.size.width)! - rect.size.width) / CGFloat(2.0)
        
        let waitingView = UIView(frame: rect)
        waitingView.backgroundColor = UIColor.clear
        waitingView.layer.cornerRadius = 8.0
        
        rect = waitingView.bounds;
        rect.size.height = CGFloat(Float(40.0) * factor);
        
        let label = UILabel(frame: rect)
        label.text = message
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: CGFloat(Float(16.0) * factor))//UIFont(name: APP_BOLD_FONT, size: CGFloat(Float(16.0) * factor))
        waitingView.addSubview(label)
        var activityIndicatorView = UIActivityIndicatorView()
        if #available(iOS 13.0, *) {
            activityIndicatorView = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.large)
        } else {
            activityIndicatorView = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.gray)
        }
        activityIndicatorView.color = UIColor.white
        
        rect = activityIndicatorView.frame;
        rect.origin.x = ((waitingView.frame.size.width - rect.size.width)/2.0);
        rect.origin.y = label.frame.size.height + 3.0;
        activityIndicatorView.frame = rect;
        
        waitingView.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
        blackView?.tag = 1010
        blackView?.addSubview(waitingView)
        view.addSubview(blackView!)
    }
}

func showAlet(message:String,Title:String,controller:UIViewController) {
    let alertMessage = UIAlertAction(title: "Ok", style: .default) { (action) in
        
    }
    
    let alert = UIAlertController(title: "\(Title)", message: "\(message)", preferredStyle: .alert)
    alert.addAction(alertMessage)
    controller.present(alert, animated: true, completion: nil)
}

func hideActivity() {
    blackView?.removeFromSuperview()
    blackView = nil
}

func addUserJSONDataToUserDefaults(userData: JSON) {
    guard let jsonString = userData.rawString() else { return }
    UserDefaults.standard.set(jsonString, forKey: userdefault().userProfile)
}

func getCachedUserJSONData() -> JSON? {
    let jsonString = UserDefaults.standard.string(forKey: userdefault().userProfile) ?? ""
    guard let jsonData = jsonString.data(using: .utf8, allowLossyConversion: false) else { return nil }
    return try? JSON(data: jsonData)
}


// convert date and time formate
func convertDateTimeFormate(dateformate:String,convertedFormate:String,date:String) -> String {
    let dateformater = DateFormatter()
    dateformater.dateFormat = dateformate
    let date = dateformater.date(from: date)
    dateformater.dateFormat = convertedFormate
    
    let convertedDate = dateformater.string(from: date ?? Date())
    
    return convertedDate
}

extension UIViewController {
    
    func showToast(message : String, font: UIFont) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = AppColors.AppColor
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
}

func downloadAudioFile(url:String,fileName:String,main:String,successBlock:@escaping (_ response: URL )->Void) {
    if main == "Default"{
        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory, in: .userDomainMask, options: .removePreviousFile)
        AF.download(url, to: destination).responseData { response in
            if let destination = response.fileURL {
                var downloadFilesURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first

                successBlock(destination)
            }
        }
    } else {
        let destination = DownloadRequest.suggestedDownloadDestination(for: .libraryDirectory, in: .allDomainsMask, options: .removePreviousFile)
        AF.download(url, to: destination).responseData { response in
            if let destination = response.fileURL {
                successBlock(destination)
            }
        }
    }
   
    
    
}
protocol TableViewInsideCollectionViewDelegate:class {
    func cellTaped(data:JSON)
}
public func calculatePercentage(value:Double,percentageVal:Double)->Double{
    let val = value * percentageVal
    return val / 100.0
}
