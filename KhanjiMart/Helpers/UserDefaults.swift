//
//  UserDefaults.swift
//  KhanjiMark
//
//  Created by Apple on 31/05/21.
//

import Foundation
import SwiftyJSON
class UserDefaultsManager {
    var isLogIn = UserDefaults.standard.value(forKey: userdefault().useIsLogIn) as? Bool {
        didSet { UserDefaults.standard.set(self.isLogIn ?? "", forKey: userdefault().useIsLogIn) }
    }
}
struct userdefault {
    let userProfile = "userProfile"
    let useIsLogIn = "isUserLogin"
}
struct userProfileKeys {
    var email = "email"
    var deletedBy = "deleted_by"
    var siteAddressPincode = "site_address_pincode"
    var gstNumber = "gst_number"
    var updatedDate = "updated_date"
    var bankAccountNumner = "bank_account_numner"
    var userType = "user_type"
    var site_address_2 = "site_address_2"
    var id = "id"
    var profilePhoto = "profile_photo"
    var deleted = "deleted"
    var supplier_code = "supplier_code"
    var bank_name = "bank_name"
    var office_address_1 = "office_address_1"
    var middle_name = "middle_name"
    var first_name = "first_name"
    var office_address_taluka = "office_address_taluka"
    var profile_type_id = "profile_type_id"
    var password = "password"
    var mobile_number = "mobile_number"
    var approval_status = "approval_status"
    var reg_key = "reg_key"
    var site_address_1 = "site_address_1"
    
    var address_1 = "address_1"
    var address_2 = "address_2"
    
    
    var bank_account_name = "bank_account_name"
    var office_address_2 = "office_address_2"
    var bank_ifsc_code = "bank_ifsc_code"
    var office_address_pincode = "office_address_pincode"
    var created_date = "created_date"
    var token = "token"
    var office_address_distict = "office_address_distict"
    var site_address_distict = "site_address_distict"
    
    var district = "district"
    var taluka = "taluka"
    var pincode = "pincode"
    
    
    var company_name = "company_name"
    var updated_by = "updated_by"
    var title = "title"
    var pan_number = "pan_number"
    var otp_verify = "otp_verify"
    var last_name = "last_name"
    var site_address_taluka = "site_address_taluka"
    var otp = "otp"
}

