//
//  NetworkManager.swift
//  RintoneMaker
//
//  Created by Apple on 22/05/21.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

var downloadFilesURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
var BaseURL = "https://www.khanijmart.com/Webservices/"
var perpage = 20

struct URLS {
    struct paytm {
        let withDrawAmount = BaseURL + "add_supplier_withdraw_request"
        let intialTokenGet = "https://www.khanijmart.com/paytm/init_Transaction.php"
        let walletAmount = BaseURL + "walletSupplierTransactionHistory"
        let couponCodeValidation = BaseURL + "coupon_code_validation"
    }
    struct Comman {
        let login = BaseURL + "login"
        let registration = BaseURL + "registration"
        let OtpVerify = BaseURL + "otpVerify"
        let GetState = BaseURL + "getState"
        let GetDistrict = BaseURL + "getDistrict"
        let GetTaluka = BaseURL + "getTaluka"
        let GetPincode = BaseURL + "getPincode"
        let ChangePassword = BaseURL + "changePassword"
        let getProductCategory = BaseURL + "productCategory"
        let getProductSubCategory = BaseURL + "productSubCategory"
      
    }
    
    struct SellerSide {
    
        let getMySupplierOrder = BaseURL + "getMySupplierOrder"
        let getMySupplierProduct = BaseURL + "supplierProductList"
        let productDelete = BaseURL + "supplierProductDelete"
        let profileUpdate = BaseURL + "supplierProfileUpdate"
        let productDetails = BaseURL + "supplierProductDetail"
        let profileImageUpload = BaseURL + "SupplierProfilePicUpload"
        let addProductList = BaseURL + "addSuppliersProductDetails"
        let productUpdate = BaseURL + "editSuppliersProductDetails"
        let orderStatusUpdate = BaseURL + "orderStatusUpdateBySupplier"
    
    }
    
    struct Consumer {
        
        let TopsupplierList = BaseURL + "getTopSupplierList"
        let TopProductCategory = BaseURL + "topProductList"
        let productList = BaseURL + "productList"
        let OfferBanner = BaseURL + "offerBanner"
        let requestQuot = BaseURL + "requestaQuote"
        let SupplierProductList = BaseURL + "supplierProductList"
        let placeOrder = BaseURL + "orderPlace"
        let getSupplierList = BaseURL + "getSupplierList"
        let getSupplierListImage = BaseURL + "getSupplierProductImages"
        let searchList = BaseURL + "search"
        let referandEarneDetails = BaseURL + "referandearndetail"
        let supplierprofileUpload = BaseURL + "SupplierProfilePicUpload"
        let supplierProfileDetails = BaseURL + "customerProfileUpdate"
        let orderList = BaseURL + "myCustomerOrder"
        let orderStatusUpdate = BaseURL + "orderStatusUpdateByCustomer"
        let cartList = BaseURL + "cartList"
        let addCart = BaseURL + "cartItemadd"
        let RemovecartList = BaseURL + "removeCartItem"
        let productDetails = BaseURL + "productDetail"
        let cancelOrder = BaseURL  + "saveCancelOrderRequestDetails"
    }
}


class NetworkManager {
    
    func getDataFromServer(url:String,method:HTTPMethod,parameter:Parameters,view:UIView,successBlock:@escaping (_ response: JSON )->Void) {
        showActivityWithMessage(message: "", inView: view)
        print("parameter \(parameter)")
        print("url \(url)")
        
        AF.request(url.replacingOccurrences(of: " ", with: "%20"), method: method, parameters: parameter, headers: nil).responseJSON  { (response ) in
            hideActivity()
            switch response.result {
            case .success:
                successBlock(JSON(response.data!))
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    func getDataWithMultiPart(endUrl: URL, parameters: Parameters, imageData:Data, successBlock:@escaping (_ response: JSON )->Void) {
       
    }
    
    
}
